const HtmlWebpackPlugin = require("html-webpack-plugin");
const webpack = require('webpack');
const { DefinePlugin } = require("webpack");
const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common.config, {
  mode: 'development',
  devtool: 'inline-source-map',
  plugins: [
    new HtmlWebpackPlugin(),
    new DefinePlugin({
      __PACKAGE_VERSION__: common.version,
    }),
  ],
  devServer: {
    static: common.outputPath,
    host: 'localhost',
    port: 3000,
    historyApiFallback: true,
    hot: true,
  },
});
