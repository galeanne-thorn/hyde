const HtmlWebpackPlugin = require("html-webpack-plugin");
const HtmlInlineScriptPlugin = require("html-inline-script-webpack-plugin");
const { DefinePlugin } = require("webpack");
const { merge } = require("webpack-merge");
const common = require("./webpack.common.js");

module.exports = merge(common.config, {
  mode: "production",
  plugins: [
    new HtmlWebpackPlugin({
      filename: "hyde.html",
      inject: "body",
    }),
    new HtmlInlineScriptPlugin(),
    new DefinePlugin({
      __PACKAGE_VERSION__: common.version,
    }),
  ]
});