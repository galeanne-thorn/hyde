import { CssBaseline } from "@material-ui/core";
import { ThemeProvider } from "@material-ui/core/styles";
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import GameApp from "./components/GameApp";
import { loadGame } from "./game";
import store from "./model/Store";
import theme from "./Theme";

const rootElement = document.getElementById("root");

try {
    const game = await loadGame();
    const app = (
        <Provider store={store}>
            <ThemeProvider theme={theme}>
                <CssBaseline />
                <GameApp game={game} />
            </ThemeProvider>
        </Provider>
    );
    ReactDOM.render(app, rootElement);
}
catch(err: any) {
    const app = (
        <div>
            <h1>Error loading game data</h1>
            <p>{err}</p>
        </div>
    );
    ReactDOM.render(app, rootElement);
}
