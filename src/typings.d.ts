/**
 * Module for parsed MD files.
 */
declare module "*.md" {
    /**
     * Markdown string compiled to HTML
     */
    declare const content : string;

    export default content;
};
