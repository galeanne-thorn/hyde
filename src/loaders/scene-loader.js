///
/// Scene loader for webpack
///

const path = require("path");

// Steps parsing regex
// Capturing groups:
// 0 : label
// 1 : command label
// 2 : params
const stepRegex = /^---(?:(.*)@)?(.)(?::(.*))?$/gm;

// Step text parsing regex
const codeRegex = /<do>(.*)<\/do>/sm;

/**
 * Parses the text to step definitions
 *
 * @param {string|Buffer} text
 * @returns
 */
function parseSteps(text) {

    // Split text to scene steps
    const parts = text.split(stepRegex);

    const steps = [];

    // First text is name of scene
    const sceneName = parts[0].trim();

    if (sceneName === "") {
        throw new Error("Scene name is required!");
    }

    for (let i = 1; i < parts.length; i += 4) {
        steps.push({
            label: parts[i]?.trim(),
            command: parts[i + 1]?.trim(),
            parameters: parts[i + 2]?.trim(),
            text: parts[i + 3]?.trim()
        });
    }

    return { sceneName, steps };
}

/**
 * Parse choice
 * @param {string} text
 */
function parseCode(text) {

    const match = text.match(codeRegex);
    if (!match) {
        return { text, code: undefined };
    }

    return {
        text: text.substr(0, match.index).trim(),
        code: match[1].trim()
    };
}

/**
 * Generates output for choice step
 * @param {Step} s
 */
function outputGoToStage(s) {

    if (s.parameters === undefined) {
        throw new Error(`Undefined stage for 'go to stage' choice!`);
    }
    const { code, text } = parseCode(s.text);

    return `
.withChoices(new G.SceneChoice(
    "${text}",
    ${code ? "\n" + code + "," : ""}
    G.setStage("${s.parameters}"))
)`;
}

/**
 * Generates output for choice step
 * @param {Step} s
 */
function outputGoToStep(s) {
    if (s.parameters === undefined) {
        throw new Error(`Undefined step for 'continue to step' choice!`);
    }
    const { code, text } = parseCode(s.text);

    return `
.withChoices(new G.SceneChoice(
    "${text}",
    ${code ? "\n" + code + "," : ""}
    G.continueScene("${s.parameters}"))
)`;
}

/**
 * Parses MD formatted text to simple scene steps without choices
 *
 * Expects scene steps to be split by following string starting
 * on the beginning of line:
 *
 * ---label@command:parameters
 *
 * If text does not start with the delimiter as above, it is assumed
 * the first step is narration
 *
 * @param {string|Buffer} text Content of the resource file
 *
 * @returns Typescript code for the scene
 */
module.exports = function (text) {

    if (text.length <= 0) {
        throw new Error("Empty text to parse!");
    }

    // Local folder
    const folder = this.context;
    const filename = this.resourcePath;

    // console.log(`Processing ${filename} in ${folder} (${this.rootContext})`);

    // TODO: Take this from options
    const modelFolder = path.resolve(this.rootContext, "src/model/game");
    const modelPath = path.relative(folder, modelFolder).replace(/\\/g, '/');

    const gameFolder = path.resolve(this.rootContext, "src/game");
    const gamePath = path.relative(folder, gameFolder).replace(/\\/g, '/');

    const { sceneName, steps } = parseSteps(text);
    const sceneId = path.basename(filename, ".scene");

    let result = `
import * as G from "${modelPath}";
import game from "${gamePath}/Game";

game
    .addStage(new G.Scene("${sceneId}", "${sceneName}")
`;

    steps.forEach(s => {
        // Set step label
        if (s.label) {
            result += `.nextStepLabel("${s.label}")\n`;
        }

        // Add step
        switch (s.command) {
            // GO TO STAGE
            case "G":
                result += outputGoToStage(s);
                break;

            // GO TO STEP
            case "J":
                result += outputGoToStep(s);
                break;

            // NARRATE
            case "N":
                result += `.narrate(\`${s.text}\`)\n`;
                break;

            // REPLY
            case "R":
                result += `.reply(\`${s.text}\`)\n`;
                break;

            // SAY
            case "S":
                if (s.parameters === undefined) {
                    throw new Error(`Undefined who for scene step Say!`);
                }
                result += `.say("${s.parameters}", \`${s.text}\`)\n`;
                break;

            default:
                throw new Error(`Unsupported scene command '${s.command}'`);
        }
    });

    result += "\n);\n"

    // console.log(`---\n${result}\n---\n`);

    return result;
}
