/**
 * Local storage interface
 */
export interface Storage {
    /**
     * Prefix used by the storage
     */
    readonly prefix: string;

    /**
     * Directly adds a value to local storage
     * If local storage is not available in the browser use cookies
     * Example use: localStorageService.add('library','angular');
     *
     * @param key - Key to store the value under
     * @param value - Value to store
     */
    save(key: string, value: any): void;

    /**
     * Directly get a value from local storage
     *
     * @param key - Key to look for
     * @returns Value stored under the key
     */
    load<T>(key: string): T | null;

    /**
     * Remove an item from local storage
     *
     * @param key - Key to remove
     */
    remove(key: string): void;

    /**
     * Returns array of keys this local storage uses
     */
    keys(): string[];

    /**
     * Remove all data for this app from local storage
     * Also optionally takes a regular expression string and removes the matching key-value pairs
     * Should be used mostly for development purposes
     */
    clearAllFromLocalStorage(regularExpression?: string): boolean;
}

/**
 * Local Storage implementation support
 *
 * Lot of code borrowed from https://github.com/grevory/angular-local-storage
 */
export default class LocalStorage implements Storage {

    /**
     * Storage prefix
     */
    public get prefix() {
        return this.storagePrefix;
    }

    /**
     * Creates new local storage instance
     *
     * @param storagePrefix - Prefix to identify records in storage
     */
    public constructor(private storagePrefix: string) {
    }

    /**
     * Directly adds a value to local storage
     * Example use: localStorageService.add('library','angular');
     *
     * @param key - Key to store the value under
     * @param value - Value to store
     */
    public save(key: string, value: any): void {
        value = this.serializeValue(value);
        localStorage.setItem(this.deriveQualifiedKey(key), value);
    }

    /**
     *  Directly get a value from local storage
     *
     * @param key - Key to look for
     * @returns Value stored under the key
     */
    public load<T>(key: string): T | null {
        const item = localStorage.getItem(this.deriveQualifiedKey(key));
        if (item === null) return null;
        return this.deserializeValue(item) as T;
    }

    /**
     * Remove an item from local storage
     *
     * @param key - Key to remove
     */
    public remove(key: string): void {
        localStorage.removeItem(this.deriveQualifiedKey(key));
    }

    /**
     * Returns array of keys this local storage uses
     */
    public keys(): string[] {

        const prefixLength = this.storagePrefix.length;
        const keys = [];
        for (const key in localStorage) {
            // Only return keys that are for this app
            if (key.startsWith(this.storagePrefix)) {
                keys.push(key.substr(prefixLength));
            }
        }
        return keys;
    }

    /**
     * Remove all data for this app from local storage
     * Also optionally takes a regular expression string and removes the matching key-value pairs
     * Should be used mostly for development purposes
     */
    public clearAllFromLocalStorage(regularExpression: string = ""): boolean {
        // accounting for the '.' in the prefix when creating a regex
        const tempPrefix = this.storagePrefix.slice(0, -1);
        const testRegex = new RegExp(tempPrefix + "." + regularExpression);

        const prefixLength = this.storagePrefix.length;
        for (const key in localStorage) {
            // Only remove items that are for this app and match the regular expression
            if (testRegex.test(key)) {
                this.remove(key.substr(prefixLength));
            }
        }
        return true;
    }

    /**
     *  Creates key with prefix
     */
    private deriveQualifiedKey(key: string): string {
        return `${this.storagePrefix}.${key}`;
    }

    /**
     * Determines whether the num is string representation of number
     */
    private isStringNumber(num: string): boolean {
        return /^-?\d+\.?\d*$/.test(num.replace(/["']/g, ""));
    }

    /**
     * Serializes value to JSON
     */
    private serializeValue(value: any): string {
        // Let's convert undefined values to null to get the value consistent
        if (value === undefined) {
            value = null;
        }
        else {
            value = JSON.stringify(value);
        }
        return value;
    }

    /**
     * Parses value from JSON
     */
    private deserializeValue(value: string): any {
        if (value === null || value === undefined) return null;
        return JSON.parse(value);
    }
}
