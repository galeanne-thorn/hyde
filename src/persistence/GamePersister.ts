import LocalStorage from "./LocalStorage";
import Persister from "./Persister";
import { GameState } from "../model/game/GameState";

/**
 * Local storage
 */
export const storage = new LocalStorage("the-strange-case-of-ms-hyde");

const persister = new Persister<GameState>(storage, 10);

/**
 * Game persister
 */
export default persister;