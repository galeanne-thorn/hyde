import { nanoid } from "nanoid";
import { Storage } from "./LocalStorage";

/**
 * Description of the save slot
 */
export interface SlotDescription {
    /** Id of the slot */
    id: string;
    /** Is auto slot */
    isAuto: boolean;
    /** Date the slot was written to */
    date: Date;
    /** Name of the slot */
    name: string;
}

/**
 * Information about all saved slots
 */
export interface SlotDescriptions {
    /** Slot descriptions */
    slots: SlotDescription[];
    /** Index of last autosave slot */
    lastAutoSave: number;
}

/** Prefix for save slots */
const DATA_PREFIX = "save-data-";
/** Prefix for save slot information */
const INFO_PREFIX = "save-info";

/**
 * Persister
 *
 * Saves data to local storage
 *
 * Data are saved in two slots:
 * * SP-save-info = description of the save slots
 * * SP-save-data-ID = serialized data
 *
 * Where SP is Storage.Prefix and ID unique identification of the slot
 */
export default class Persister<TData> {

    #storageInfo: SlotDescriptions;

    /**
     * Gets whether there is any save slot
     */
    public get anySaveExists(): boolean {
        return this.#storageInfo.slots.length > 0;
    }

    /**
     * Gets list of existing save slots
     *
     * @returns Array of slots. If the slot is not used, the return[n] will be undefined
     */
    public get saveSlots(): SlotDescriptions {
        return this.#storageInfo;
    }

    /**
     * Creates new Persister
     *
     * @param storage - Persistent storage to use
     * @param maxAutosaveSlots - Number of autosave slots. This is only used if the number is not read from storage
     */
    public constructor(private storage: Storage, private maxAutosaveSlots: number = 8) {

        const rawSavedInfo = storage.load<SlotDescriptions>(INFO_PREFIX);
        // Make sure the dates are of type Date
        rawSavedInfo?.slots.forEach(s => s.date = new Date(s.date));

        this.#storageInfo = rawSavedInfo || {
            slots: [],
            lastAutoSave: maxAutosaveSlots - 1
        };
    }

    /**
     * Saves data to new slot with given name and slot Id
     *
     * @param data - Data to save
     * @param name - Name for UI of the save slot
     */
    public save(data: TData, name: string): void {
        let slotId = nanoid();
        while (this.#storageInfo.slots.find(s => s.id === slotId)) {
            // prevent the improbable duplicate
            slotId = nanoid();
        }
        this.#storageInfo.slots.push(
            {
                id: slotId,
                isAuto: false,
                name,
                date: new Date()
            }
        );
        this.saveStorageInfo();
        this.storage.save(DATA_PREFIX + slotId, data);
    }

    /*
    * Auto saves data to local storage
     * @param data - Data to save
     */
    public autoSave(data: TData, name: string): void {
        const nextSave = (this.#storageInfo.lastAutoSave + 1) % this.maxAutosaveSlots;
        this.#storageInfo.lastAutoSave = nextSave;
        const id = `auto-${nextSave}`;

        const slot = this.#storageInfo.slots.find(s => s.id === id);
        if (slot) {
            slot.isAuto = true;
            slot.name = `Autosave ${nextSave + 1}: ${name}`;
            slot.date = new Date();
        }
        else {
            this.#storageInfo.slots.push({
                id,
                isAuto: true,
                name: `Autosave ${nextSave + 1}: ${name}`,
                date: new Date()
            });
        }
        this.saveStorageInfo();
        this.storage.save(DATA_PREFIX + id, data);
    }

    /**
     * Loads data from given slot
     */
    public load(slotId: string): TData {
        return this.storage.load(DATA_PREFIX + slotId) as TData;
    }

    /**
     * Load latest saved game
     */
    public loadLatest(): TData {

        if (!this.anySaveExists) {
            throw new Error("No saved game exists!");
        }

        // Find newest auto slot save
        let date = new Date("1900-01-01");
        let slotId: string | undefined = undefined;
        for (const slot of this.#storageInfo.slots) {
            if (slot.date > date) {
                date = slot.date;
                slotId = slot.id
            }
        }
        if (slotId === undefined) {
            throw new Error("No saved game found, strange!");
        }

        return this.load(slotId);
    }

    /**
     * Clears save slot
     */
    public clear(slotId: string): void {
        // Delete data
        this.storage.remove(DATA_PREFIX + slotId);
        // Try to remove slot info
        let index = this.#storageInfo.slots.findIndex(s => s.id === slotId);
        if (index < 0) return;

        this.#storageInfo = {
            ...this.#storageInfo,
            slots: [
                ...this.#storageInfo.slots.slice(0, index),
                ...this.#storageInfo.slots.slice(index + 1)
            ]
        };
        this.saveStorageInfo();
    }

    private saveStorageInfo(): void {
        this.storage.save(INFO_PREFIX, this.#storageInfo);
    }
}
