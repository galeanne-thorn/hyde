import { AnyAction } from "redux";
import persister from '../persistence/GamePersister';
import { AppState } from "./AppState";
import { UiMode } from "./mainUi/UiState";
import { registerAction } from "./RootReducer";
import { updateLoadedGame } from "./updateLoadedGame";

export const ACTION_LOAD_LATEST = "LOAD_LATEST";

/**
 * Creates action to load latest saved game
 * @returns
 */
export default function loadLatestGame(): AnyAction {
    return {
        type: ACTION_LOAD_LATEST
    }
}

/**
 * Handler for the Load Latest Game action
 * @param state
 * @param action
 */
function handler(state: AppState): AppState {

    const loadedState = persister.loadLatest();
    return {
        ui: {
            ...state.ui,
            gameInProgress: true,
            mode: UiMode.Game,
        },
        game: updateLoadedGame(loadedState)
    };
}

registerAction(ACTION_LOAD_LATEST, handler);