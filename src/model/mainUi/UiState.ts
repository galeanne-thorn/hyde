/**
 * Main UI States
 */
export enum UiMode {
    MainMenu,
    Game,
    OptionsMenu,
    AboutMenu,
    SaveMenu
}

/**
 * Ui State
 */
export interface UiState {
    /**
     * UI mode
     */
    mode: UiMode;
    /**
     * is any game in progress?
     */
    gameInProgress: boolean,
}

/**
 * Gets initial UI state
 */
export function getInitialUiState(): UiState {
    return {
        mode: UiMode.MainMenu,
        gameInProgress: false,
    };
}
