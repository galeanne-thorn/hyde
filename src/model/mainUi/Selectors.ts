import { useApp } from "../Selectors";
import { UiState } from "./UiState";

/**
 * Typed game state selector
 */
export type UiStateSelector<T> = (state: UiState) => T;

/**
 * Game state hook
 */
export function useUi<T>(selector: UiStateSelector<T>): T {
    return useApp(state => selector(state.ui)) as T;
}
