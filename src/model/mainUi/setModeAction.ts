import { AnyAction } from "redux";
import { AppState } from "../AppState";
import { registerAction } from "../RootReducer";
import { UiMode } from "./UiState";

const ACTION_TYPE = "SET_MODE";

/**
 * Creates action to start new game
 * @returns
 */
export default function setMode(mode: UiMode): AnyAction {
    return {
        type: ACTION_TYPE,
        mode,
    }
}

/**
 * Set Mode type
 */
export type SetModeAction = ReturnType<typeof setMode>;

/**
 * Handler for the Start New Game action
 * @param state
 * @param action
 */
function handler(state: AppState, action: SetModeAction): AppState {
    return {
        ...state,
        ui: {
            ...state.ui,
            mode: action.mode,
        },
    };
}

registerAction(ACTION_TYPE, handler);