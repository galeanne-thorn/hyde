import { AnyAction } from "redux";
import { UiState } from "./UiState";
import { registerAction } from "../RootReducer";

/**
 * UI reducer
 */
export interface UiActionHandler {
    (state: UiState, action: AnyAction): UiState;
}

/**
 * Registers action handler for game reducer
 * @param type
 * @param handler
 */
export function registerUiAction(
    type: string,
    handler: UiActionHandler
): void {
    registerAction(
        type,
        (s, a) => ({
            ...s,
            ui: handler(s.ui, a)
        })
    );
}