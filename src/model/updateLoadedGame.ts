import game from "../game/Game";
import { GameState } from "./game";

/**
 * Updates loaded game to the latest version.
 */
export function updateLoadedGame(loadedState: GameState): GameState {

    const init = game.createInitialState();

    // TODO: Compare versions

    // Create missing NPC states
    for (const id in init.npcs) {
        const npc = init.npcs[id];
        if (loadedState.npcs[id] === undefined) {
            loadedState.npcs[id] = npc;
        }
    }

    return loadedState;
}