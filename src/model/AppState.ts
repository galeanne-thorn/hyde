import game from "../game/Game";
import { GameState } from "./game/GameState";
import { getInitialUiState, UiState } from "./mainUi/UiState";

/**
 * State of the application
 */
export interface AppState {
    /**
     * Main UI state
     */
    ui: UiState;

    /**
     * Current game state
     */
    game: GameState;
}

/**
 * Initial state
 */
export const initialState: AppState = {
    ui: getInitialUiState(),
    game: game.createInitialState()
}
