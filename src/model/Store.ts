/**
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { applyMiddleware, createStore } from 'redux';
import logger from "redux-logger";
import autoSaver from './AutoSaverMiddleware';
import rootReducer from './RootReducer';
import { composeWithDevTools } from 'redux-devtools-extension';

const store = createStore(
    rootReducer,
    composeWithDevTools(
        applyMiddleware(autoSaver, logger)
    )
);

/**
 * Type of dispatch function
 */
export type GameDispatch = typeof store.dispatch;

/**
 * Game Redux Store
 */
export default store;
