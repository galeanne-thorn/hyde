import { AnyAction } from "redux";
import game from "../game/Game";
import persister from '../persistence/GamePersister';
import { AppState } from "./AppState";
import { UiMode } from "./mainUi/UiState";
import { registerAction } from "./RootReducer";
import { updateLoadedGame } from "./updateLoadedGame";

export const ACTION_LOAD = "LOAD";

/**
 * Creates action to load saved game
 * @returns
 */
export default function loadGame(slotId: string): AnyAction {
    return {
        type: ACTION_LOAD,
        slotId,
    }
}

/**
 * Type of Load Game action
 */
export type LoadGameAction = ReturnType<typeof loadGame>;

/**
 * Handler for the Load Game action
 * @param state
 * @param action
 */
function handler(state: AppState, action: LoadGameAction): AppState {

    const loadedState = persister.load(action.slotId);

    return {
        ui: {
            ...state.ui,
            gameInProgress: true,
            mode: UiMode.Game,
        },
        game: updateLoadedGame(loadedState)
    };
}

registerAction(ACTION_LOAD, handler);