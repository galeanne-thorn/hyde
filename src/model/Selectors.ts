import { useSelector } from "react-redux";
import { AppState } from "./AppState";

/**
 * Typed game state selector
 */
 export type AppStateSelector<T> = (state: AppState) => T;

 /**
  * Application state hook
  */
 export function useApp<T>(s: AppStateSelector<T>): T {
     return useSelector(s) as T;
 }

