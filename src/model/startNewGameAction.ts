import { AnyAction } from "redux";
import game from "../game/Game";
import { AppState } from "./AppState";
import { UiMode } from "./mainUi/UiState";
import { registerAction } from "./RootReducer";

const ACTION_TYPE = "NEW_GAME";

/**
 * Creates action to start new game
 * @returns
 */
export default function startNewGame(): AnyAction {
    return {
        type: ACTION_TYPE
    }
}

/**
 * Handler for the Start New Game action
 * @param state
 * @param action
 */
function handler(state: AppState): AppState {
    return {
        ui: {
            ...state.ui,
            gameInProgress: true,
            mode: UiMode.Game,
        },
        game: game.createInitialState()
    };
}

registerAction(ACTION_TYPE, handler);