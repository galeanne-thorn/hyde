import { GameEvent } from "./GameEvent";
import { GameState } from "./GameState";
import { Npc } from "./person";
import Hero from "./person/Hero";
import { Quest } from "./quest";
import Stage from "./Stage";

/**
 * Game template
 */
export class Game {

    /**
     * Game stages
     */
    private readonly _stages: Map<string, Stage> = new Map();

    /**
     * Humans catalog
     */
    private readonly _npcs: Map<string, Npc> = new Map();

    /**
     * Hero data
     */
    public readonly hero: Hero = new Hero();

    /**
     * Quests
     */
    private readonly _quests: Map<string, Quest> = new Map();

    /**
     * Events
     */
    private readonly _events: Array<GameEvent> = [];

    /**
     * Game constructor
     * @param name Name of the game
     * @param author Author of the game
     */
    public constructor(public readonly name: string) {
    }

    /**
     * Creates initial game state
     */
    public createInitialState(): GameState {
        const state: GameState = {
            // First game stage to start with is "start"
            stages: ["start"],
            scenes: {},
            hero: this.hero.createState(),
            npcs: {},
            quests: {},
            time: {
                day: 1,
                hour: 18,
                minute: 0
            },
        };
        this._npcs.forEach(n => state.npcs[n.id] = n.createState());
        this._quests.forEach(n => state.quests[n.id] = n.createState());
        return state;
    }

    /**
     * Adds stage to the game
     * @param state Stage to add
     * @returns This Game
     */
    public addStage(stage: Stage): Game {
        if (this._stages.has(stage.id)) {
            throw new Error(`Stage '${stage.id}' already exists!`);
        }
        this._stages.set(stage.id, stage);
        return this;
    }

    /**
     * Gets stage
     * @param id Id of the stage to get
     */
    public getStage<TStage extends Stage>(id: string): TStage {
        const result = this._stages.get(id);
        if (result === undefined) {
            throw new Error(`Stage ${id} not found!`);
        }
        return result as TStage;
    }

    /**
     * Adds human to the game
     * @param state Human to add
     * @returns This Game
     */
    public addNpc(npc: Npc): Game {
        if (this._npcs.has(npc.id)) {
            throw new Error(`NPC '${npc.id}' already exists!`);
        }
        this._npcs.set(npc.id, npc);
        npc.personaScenes.forEach(s => this.addStage(s));
        return this;
    }

    /**
     * Gets human
     * @param id Id of the human to get
     */
    public getNpc<TNpc extends Npc = Npc>(id: string): TNpc {
        const result = this._npcs.get(id);
        if (result === undefined) {
            throw new Error(`NPC '${id}' not found!`);
        }
        return result as TNpc;
    }

    /**
     * Adds quest to the game
     * @param state Quest to add
     * @returns This Game
     */
    public addQuest(quest: Quest): Game {
        if (this._quests.has(quest.id)) {
            throw new Error(`Quest '${quest.id}' already exists!`);
        }
        this._quests.set(quest.id, quest);
        return this;
    }

    /**
     * Gets quest
     * @param id Id of the quest to get
     */
    public getQuest<TQuest extends Quest = Quest>(id: string): TQuest {
        const result = this._quests.get(id);
        if (result === undefined) {
            throw new Error(`Quest '${id}' not found!`);
        }
        return result as TQuest;
    }

    /**
     * Adds event to game
     * @param event
     */
    public addEvent(event: GameEvent) {
        this._events.push(event);
    }

    /**
     * Gets all events triggered by current state
     * @param state
     * @returns
     */
    public getTriggeredEvents(state: GameState) : Array<GameEvent> {
        return this._events
          .filter(e => e.condition(state, this));
    }
}
