import { combine, GameHandler } from "../GameHandler";
import { QuestTaskState } from "./QuestState";
import { QuestStatus } from "./QuestStatus";

/**
 * Represents one task in the quest
 */
export class QuestTask {

    /**
     * What to be done when task succeeds
     */
    public onSuccess: GameHandler | undefined = undefined;
    /**
     * What to be done when task fails
     */
    public onFail: GameHandler | undefined = undefined;

    /**
     * Creates new quest task
     * @param id - Id of the subtask
     * @param description - Description of the task
     * @param active - Indicates whether task is active by default
     */
    public constructor(
        public readonly id: string,
        public readonly description: string,
        public readonly active: boolean = false
    ) { }

    /**
     * Creates task status
     * @returns
     */
    public createState(): QuestTaskState {
        return {
            status: this.active
                ? QuestStatus.Active
                : QuestStatus.Inactive
        };
    }

    /**
     * Adds handler to execute when task is finished successfully
     * @param handler
     * @returns This task
     */
    public onSuccessDo(handler: GameHandler, ...handlers: GameHandler[]): this {
        if (this.onSuccess !== undefined) {
            throw new Error(`OnSuccess handler for Quest Task ${this.id} is already specified!`);
        }
        this.onSuccess = combine(handler, ...handlers);
        return this;
    }

    /**
     * Adds handler to execute when task fails
     * @param handler
     * @returns This task
     */
    public onFailDo(handler: GameHandler, ...handlers: GameHandler[]): this {
        if (this.onFail !== undefined) {
            throw new Error(`OnFail handler for Quest Task ${this.id} is already specified!`);
        }
        this.onFail = combine(handler, ...handlers);
        return this;
    }
}
