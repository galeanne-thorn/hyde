/**
 * Quest or task state
 */
export enum QuestStatus {
    /**
     * Is inactive
     */
    Inactive = 0,
    /**
     * Is active (in progress)
     */
    Active = 1,
    /**
     * Finished successfully
     */
    Success = 2,
    /**
     * Failed
     */
    Failed = 3,
}
