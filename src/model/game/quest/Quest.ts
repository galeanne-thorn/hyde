import { Draft } from "immer";
import { Game } from "../Game";
import { combine, GameHandler } from "../GameHandler";
import { GameState } from "../GameState";
import { QuestState } from "./QuestState";
import { QuestStatus } from "./QuestStatus";
import { QuestTask } from "./QuestTask";

/**
 * Represents quest
 */
export class Quest {
    /**
     * Tasks this quest is made of
     */
    public readonly tasks: Map<string, QuestTask> = new Map();

    /**
     * What to be done when task succeeds
     */
    public onSuccess: GameHandler | undefined = undefined;
    /**
     * What to be done when task fails
     */
    public onFail: GameHandler | undefined = undefined;

    /**
     * Creates new quest
     *
     * @param id - Id of the quest
     * @param description - Description of the quest
     * @param active - Is the quest active by default
     */
    public constructor(
        public readonly id: string,
        public readonly description: string,
        public readonly active: boolean = false
    ) {
    }

    /**
     * Creates new quest state
     * @param gameState
     */
    public createState(): QuestState {
        const state: QuestState = {
            status: this.active
                ? QuestStatus.Active
                : QuestStatus.Inactive,
            tasks: {},
        };
        this.tasks.forEach(n => state.tasks[n.id] = n.createState());
        return state;
    }

    /**
     * Gets quest state
     * @param gameState
     */
    public getState(gameState: GameState): Draft<QuestState> {
        const state = gameState.quests[this.id];
        if (state === undefined) {
            throw new Error()
        }
        return state;
    }

    /**
     * Adds task to quest
     * @param task - Task to add
     * @returns This quest
     */
    public withTask(task: QuestTask): this {
        if (this.tasks.has(task.id)) {
            throw new Error(`Quest ${this.id} already has task with id ${task.id}`);
        }
        this.tasks.set(task.id, task);
        return this;
    }

    /**
     * Adds handler to execute when task is finished successfully
     * @param handler
     * @returns This task
     */
    public onSuccessDo(handler: GameHandler, ...handlers: GameHandler[]): this {
        if (this.onSuccess !== undefined) {
            throw new Error(`OnSuccess handler for Quest Task ${this.id} is already specified!`);
        }
        this.onSuccess = combine(handler, ...handlers);
        return this;
    }

    /**
     * Adds handler to execute when task fails
     * @param handler
     * @returns This task
     */
    public onFailDo(handler: GameHandler, ...handlers: GameHandler[]): this {
        if (this.onFail !== undefined) {
            throw new Error(`OnFail handler for Quest Task ${this.id} is already specified!`);
        }
        this.onFail = combine(handler, ...handlers);
        return this;
    }

    /**
     * Activates task of the quest.
     * If quest is already active or even finished, does nothing
     * @param gameState - Current game state
     * @param taskId - Id of the task to finish
     */
    public activateTask(gameState: Draft<GameState>, taskId: string) {
        const task = this.tasks.get(taskId);
        if (task === undefined) {
            throw new Error(`Quest ${this.id} does not have task with id ${taskId}`)
        }
        const questState = this.getState(gameState);
        const taskState = questState.tasks[taskId];
        if (taskState.status === QuestStatus.Inactive) {
            taskState.status = QuestStatus.Active;
            questState.status = QuestStatus.Active;
        }
        gameState.quests[this.id] = questState;
    }

    /**
     * Finishes task of the quest
     * @param gameState - Current game state
     * @param taskId - Id of the task to finish
     * @param success - Whether the task was success or failure.
     *      If not provided, assumes success
     */
    public finishTask(gameState: Draft<GameState>, game: Game, taskId: string, success: boolean = true) {
        const task = this.tasks.get(taskId);
        if (task === undefined) {
            throw new Error(`Quest ${this.id} does not have task with id ${taskId}`)
        }
        const questState = this.getState(gameState) ?? this.createState;
        const taskState = questState.tasks[taskId];
        if (taskState.status !== QuestStatus.Active) {
            throw new Error(`Quest ${this.id}, task ${taskId} is not active`)
        }

        // Finish task
        if (success) {
            taskState.status = QuestStatus.Success;
            gameState.quests[this.id] = questState;
            task.onSuccess?.(gameState, game);
        }
        else {
            taskState.status = QuestStatus.Failed;
            gameState.quests[this.id] = questState;
            task.onFail?.(gameState, game);
        }

        // Check if all tasks are done
        let allDone = true;
        let isSuccess = true;
        for (const key in questState.tasks) {
            if (Object.hasOwnProperty.call(questState.tasks, key)) {
                const t = questState.tasks[key];
                if (t.status === QuestStatus.Failed) {
                    isSuccess = false;
                }
                if (t.status === QuestStatus.Inactive
                    || t.status === QuestStatus.Active
                ) {
                    allDone = false;
                    break;
                }
            }
        }

        if (allDone) {
            if (isSuccess) {
                questState.status = QuestStatus.Success;
                gameState.quests[this.id] = questState;
                this.onSuccess?.(gameState, game);
            }
            else {
                questState.status = QuestStatus.Failed;
                gameState.quests[this.id] = questState;
                this.onFail?.(gameState, game);
            }
        }
    }
}
