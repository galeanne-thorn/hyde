import { Condition } from "../Condition";
import { GameHandler } from "../GameHandler";
import { QuestStatus } from "./QuestStatus";

/**
 * Creates handler to activate quest task
 * @param questId - Quest id
 * @param taskId - Task id
 */
export const activateQuestTask =
    (questId: string, taskId: string): GameHandler => (s, g) => {
        const quest = g.getQuest(questId);
        quest.activateTask(s, taskId);
    };

/**
 * Creates handler to finish quest task
 * @param questId - Quest id
 * @param taskId - Task id
 * @param success - Whether to succeed or fail the task
 */
const finishQuestTask =
    (questId: string, taskId: string, success: boolean): GameHandler => (s, g) => {
        const quest = g.getQuest(questId);
        quest.finishTask(s, g, taskId, success);
    };

/**
 * Creates handler to succeed quest task
 * @param questId - Quest id
 * @param taskId - Task id
 */
export const succeedQuestTask = (questId: string, taskId: string) =>
    finishQuestTask(questId, taskId, true);

    /**
 * Creates handler to fail quest task
 * @param questId - Quest id
 * @param taskId - Task id
 */
export const failQuestTask = (questId: string, taskId: string) =>
    finishQuestTask(questId, taskId, false);

/**
 * Checks whether quest is active
 * @param questId
 * @param taskId
 * @returns
 */
export const isQuestActive = (questId: string): Condition => (s) => {
    const questState = s.quests[questId];
    if (questState === undefined) {
        throw new Error(`Quest '${questId}' does not exist`);
    }
    return questState.status === QuestStatus.Active;
};

/**
 * Checks whether quest task is active
 * @param questId
 * @param taskId
 * @returns
 */
export const isTaskActive = (questId: string, taskId: string): Condition => (s) => {
    const questState = s.quests[questId];
    if (questState === undefined) {
        throw new Error(`Quest '${questId}' does not exist`);
    }
    const taskState = questState.tasks[taskId];
    if (taskState === undefined) {
        throw new Error(`Task '${taskId}' in quest '${questId}' does not exist`);
    }
    return taskState.status === QuestStatus.Active;
};