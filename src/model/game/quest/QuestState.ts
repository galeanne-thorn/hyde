import { QuestStatus } from "./QuestStatus";

/**
 * Quest task status
 */
export interface QuestTaskState {
    /**
     * Status of the task
     */
    readonly status: QuestStatus;
}

/**
 * Quest status
 */
export interface QuestState {
    /**
     * Status of the quest
     */
    readonly status: QuestStatus;

    /**
     * States of the quest tasks
     */
    readonly tasks: Record<string, QuestTaskState>;
}
