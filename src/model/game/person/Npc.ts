import { Draft } from "immer";
import Choice from "../Choice";
import { GameState } from "../GameState";
import { Scene } from "../scene";
import setStage from "../setStage";
import { Persona, PersonaState } from "./Persona";

/**
 * Human state
 */
export interface NpcState {
    /**
     * Id of active persona
     */
    readonly activePersona: string;

    /**
     * State of personas
     */
    readonly personas: Record<string, PersonaState>;

    /**
     * Location of the person
     */
    readonly location: string | undefined;
}

/**
 * Representation of NPC
 */
export class Npc {

    public readonly personas: Map<string, Persona> = new Map();

    /**
     * Default location
     */
    private location: string | undefined;

    /**
     * Persona choices
     */
    private readonly personaChoices: Map<string, Choice> = new Map();

    /**
     * Persona scenes
     */
    public readonly personaScenes: Map<string, Scene> = new Map();

    /**
     * Gets NPC choices
     */
    public get choices(): Array<Choice> {
        const result: Array<Choice> = [];
        this.personaChoices
            .forEach(c => result.push(c));
        return result;
    }

    /**
     * Creates new human
     *
     * @param id - Id of the human
     * @param defaultPersona - Default persona
     */
    public constructor(
        public readonly id: string,
        public readonly defaultPersona: Persona
    ) {
        this.personas.set(defaultPersona.id, defaultPersona);
        this.addPersonaChoice(defaultPersona);
    }

    /**
     * Gets Id for persona dialog
     * @param personaId
     * @returns
     */
    public getDialogId(personaId: string) {
        return `dialog-${this.id}-${personaId}`;
    }

    /**
     * Builds dialog scene for given persona
     * @param personaId - Id of the persona
     * @param sceneBuilder - Callback used to build the scene
     * @returns
     */
    public withDialogFor(personaId: string, sceneBuilder: (scene: Scene) => void): this {
        const persona = this.personas.get(personaId);
        if (persona === undefined) {
            throw new Error(`NPC '${this.id}' does not have persona '${personaId}'`);
        }
        if (this.personaScenes.has(personaId)) {
            throw new Error(`NPC '${this.id}', persona '${personaId}' already has dialog defined`);

        }
        const dialog = new Scene(this.getDialogId(personaId), `Dialog with ${persona.lastName}`);
        this.personaScenes.set(personaId, dialog);
        sceneBuilder(dialog);

        return this;
    }

    /**
     * Adds dialog choice for persona
     * @param persona
     */
    private addPersonaChoice(persona: Persona) {
        this.personaChoices.set(
            persona.id,
            new Choice(
                `npc-${this.id}-${persona.id}-dialog`,
                `Talk to {%=g.getNpc('${this.id}').getActivePersonaName(s) %}`,
                setStage(this.getDialogId(persona.id))
            )
        );
    }

    /**
     * Creates new NPC state
     */
    public createState(): NpcState {
        // Add main state
        const state: NpcState = {
            activePersona: this.defaultPersona.id,
            personas: {},
            location: this.location,
        };
        // Add persona states
        this.personas.forEach((v, k) => {
            state.personas[k] = v.createState();
        });
        return state;
    }

    /**
     * Verifies that state exists for this stage
     * and creates new one if required
     * @param gameState - Game state
     */
    public getState(gameState: GameState): Draft<NpcState> {
        const state = gameState.npcs[this.id];
        if (state === undefined) {
            throw new Error(`NPC '${this.id}' state not found`);
        }
        return state;
    }

    /**
     * Gets state for given persona.
     * If persona is not specified, uses active persona
     *
     * @param gameState - Game state
     * @param personaId - Id of the persona state to return
     */
    public getPersonaState(
        gameState: GameState,
        personaId: string | undefined = undefined
    ): Draft<PersonaState> {
        const state = this.getState(gameState);
        const id = personaId ?? state.activePersona;
        let personaState = state.personas[id];
        if (personaState === undefined) {
            throw new Error(`Persona ${id} for NCP ${this.id} not found`);
        }
        return personaState;
    }

    /**
     * Gets the active persona for the NPC
     * @param gameState - Current game state
     */
    public getActivePersona(gameState: GameState): Persona {
        const state = this.getState(gameState);
        const persona = this.personas.get(state.activePersona);
        if (persona === undefined) {
            throw new Error(`NPC ${this.id} does not have persona ${state.activePersona}`);
        }
        return persona;
    }

    /**
     * Gets active persona name
     * @param state - Current game state
     * @returns Name of the persona
     */
    public getActivePersonaName(gameState: GameState) {
        const state = this.getState(gameState);
        const personaState = state.personas[state.activePersona];
        const persona = this.personas.get(state.activePersona);
        if (persona === undefined) {
            throw new Error(`NPC ${this.id} does not have persona ${state.activePersona}`);
        }

        return personaState.isKnown
            ? personaState.isClose
                ? persona.firstName ?? persona.lastName
                : `${persona.title ?? ""} ${persona.lastName}`
            : persona.strangerName ?? "Stranger";
    }

    /**
     * Gets dialog choice for active persona
     * @param gameState - Current game state
     */
    public getDialogChoice(gameState: GameState): Choice {
        const state = this.getState(gameState);
        var choice = this.personaChoices.get(state.activePersona);
        if (choice === undefined) {
            throw new Error(`NPC ${this.id} does not have choice for persona ${state.activePersona}`);
        }
        return choice;
    }

    /**
     * Adds persona to NPC
     * @param persona - Persona to add
     * @returns - This NPC
     */
    public withPersona(persona: Persona): this {
        if (this.personas.has(persona.id)) {
            throw new Error(`NPC ${this.id} already has persona ${persona.id} !`);
        }
        this.personas.set(persona.id, persona);
        this.addPersonaChoice(persona);
        return this;
    }

    /**
     * Sets initial NPC location
     * @param roomId
     * @returns - This NPC
     */
    public atLocation(roomId: string): this {
        if (this.location !== undefined) {
            throw new Error(`NPC ${this.id} has location already set to ${this.location}`);
        }
        this.location = roomId;
        return this;
    }
}

export default Npc;