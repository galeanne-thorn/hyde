import { AverageAttributes, PersonAttributes } from "./PersonAttributes";

/**
 * State of persona
 */
export interface PersonaState {
    /**
     * Is this persona known to hero
     */
    readonly isKnown: boolean;
    /**
     * Is this persona close to hero
     * (i.e. should familiar name be used in dialogs)
     */
    readonly isClose: boolean;
}

/**
 * Persona options
 */
export interface PersonaOptions {
    /** First (given) name */
    firstName?: string,
    /** Title (Mr., Dr.) */
    title?: string,
    /** Name to use if persona is not known */
    strangerName?: string,
}

/**
 * Persona represents human in game.
 */
export class Persona {

    /**
     * Avatar url to use for this persona
     */
    public avatarUrl: string | undefined;

    /**
     * Attributes
     */
    public attributes: PersonAttributes = AverageAttributes;

    /**
     * Is persona known by default?
     */
    public isKnown: boolean = false;

    /**
     * Is persona close to hero by default?
     */
    public isClose: boolean = false;

    /**
     * Persona first name
     */
    public firstName: string | undefined;

    /**
     * Persona title
     */
    public title: string | undefined;

    /**
     * Name to use if persona is not known
     */
    public strangerName: string | undefined;

    /**
     * Creates new persona
     * @param id - Id of the persona
     * @param lastName - Last name of the persona
     * @param firstName - Name of the persona
     * @param title - Title of the persona (Mr., Ms.)
     * @param strangerName - Name to use when person is not known.
     *  If not defined, "stranger" will be used
     */
    public constructor(
        public readonly id: string,
        public readonly lastName: string,
        {
            firstName,
            title,
            strangerName
        }: PersonaOptions = {}
    ) {
        this.firstName = firstName;
        this.title = title;
        this.strangerName = strangerName;
    }

    /**
     * Creates new persona state
     */
    public createState(): PersonaState {
        // Add main state
        const state: PersonaState = {
            isKnown: this.isKnown,
            isClose: this.isClose,
        };
        return state;
    }

    /**
     * Sets the persona as known to hero by default
     * @returns This persona
     */
    public asKnown(): this {
        this.isKnown = true;
        return this;
    }

    /**
     * Sets the persona as close to hero by default.
     * Note this also sets the person as isKnownByDefault
     * @returns This persona
     */
    public asClose(): this {
        this.isKnown = true;
        this.isClose = true;
        return this;
    }

    /**
     * Ads avatar URL to persona
     * @param avatarUrl - Avatar URL
     * @returns This persona
     */
    public withAvatar(avatarUrl: string): this {
        this.avatarUrl = avatarUrl;
        return this;
    }

    /**
     * Sets persona attributes.
     * @param attributes - Attributes to set
     * @returns This persona
     */
    public withAttributes(attributes: PersonAttributes): this {
        this.attributes = attributes;
        return this;
    }
}

export default Persona;