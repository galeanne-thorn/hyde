import { GameHandler } from "../GameHandler";
import { addGuineas, addMoney, addPennies, addPounds, addShillings, Money } from "./Money";

/**
 * Adds money to hero wallet
 *
 * @param value How much to add
 */
export const addHeroMoney = (value: Money): GameHandler => s => {
    s.hero.wallet = addMoney(s.hero.wallet, value);
}

/**
 * Adds pennies to hero wallet
 * @param wallet
 * @param value
 * @returns
 */
export const addHeroPennies = (value: number): GameHandler => s => {
    s.hero.wallet = addPennies(s.hero.wallet, value);
}

/**
 * Adds shillings to hero wallet
 * @param wallet
 * @param value
 * @returns
 */
export const addHeroShillings = (value: number): GameHandler => s => {
    s.hero.wallet = addShillings(s.hero.wallet, value);
}

/**
 * Adds pounds to hero wallet
 * @param wallet
 * @param value
 * @returns
 */
export const addHeroPounds = (value: number): GameHandler => s => {
    s.hero.wallet = addPounds(s.hero.wallet, value);
}

/**
 * Adds guineas to hero wallet
 * @param wallet
 * @param value
 * @returns
 */
export const addHeroGuineas = (value: number): GameHandler => s => {
    s.hero.wallet = addGuineas(s.hero.wallet, value);
}
