import game from "../../../game/Game";
import { GameHandler } from "../GameHandler";
import { STAGE_ROOM } from "../rooms";

/**
 * Game handler that sets NPC location
 * @param npcId - Id of the NPC
 * @param room - Id of the room to set
 * @returns
 */
export const setNpcLocation = (npcId: string, room: string | undefined): GameHandler => n => {
    const npc = n.npcs[npcId];
    if (npc === undefined) {
        throw new Error(`NPC ${npcId} not found`);
    }
    // Verify room exists
    if (room !== undefined) {
        const target = game.getStage(room);
        if (target.type !== STAGE_ROOM) {
            throw new Error(`Cannot set NPC '${npcId}' location, '${room}' is not room.`)
        }
    }
    npc.location = room;
}

