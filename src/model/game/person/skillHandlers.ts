import { GameHandler } from "../GameHandler";
import { HeroSkills } from "./HeroSkills";
import { PersonAttributes } from "./PersonAttributes";

/**
 * Increases hero's experience in some skill
 * @param skill
 * @param experience
 * @returns
 */
export const increaseHeroSkill =
    (skill: keyof HeroSkills, experience: number = 1): GameHandler => s =>
        s.hero.skills[skill] += experience;

/**
 * Increases hero's experience in some attribute
 * @param attribute
 * @param experience
 * @returns
 */
export const increaseHeroAttribute =
    (attribute: keyof PersonAttributes, experience: number = 1): GameHandler => s =>
        s.hero.attributes[attribute] += experience;