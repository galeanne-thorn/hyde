/**
 * Skill and attribute levels
 */
export enum Level {
    abysmal = 0,
    terrible = 1,
    poor = 2,
    mediocre = 3,
    average = 4,
    fair = 5,
    good = 6,
    great = 7,
    superb = 8,
    epic = 9,
    legendary = 10
}

/**
 * Skills difficulty
 */
const difficulty = 2;

/**
 * Converts experience to level
 */
export function experienceToSkill(experience: number): number {
    const base = Math.floor(experience / difficulty);
    return Math.floor(Math.log(base) / Math.LN2);
}

/**
 * Calculates minimal experience needed for given level
 */
export function skillToExperience(level: number): number {
    const base = Math.round(Math.exp(level * Math.LN2));
    return base * difficulty;
}

/**
 * Default skill level
 */
 export const defaultSkillLevel = Level.poor;

 /**
  * Experience corresponding to default skill level
  */
 export const defaultSkillExperience = skillToExperience(defaultSkillLevel);
