import { GameHandler } from "../GameHandler";

/**
 * Sets persona of given NPC as known to hero
 * @param npcId - Id of the NPC
 * @param personaId - Id of the persona. If not specified, the active persona is used
 * @returns
 */
export const makePersonaKnown = (
    npcId: string,
    personaId: string | undefined = undefined
): GameHandler => n => {

    const npcState = n.npcs[npcId];
    if (npcState === undefined) {
        throw new Error(`NPC ${npcId} not found`);
    }
    const id = personaId ?? npcState.activePersona;
    const personaState = npcState.personas[id];
    if (personaState === undefined) {
        throw new Error(`Persona ${id} of NPC ${npcId} not found`);
    }

    personaState.isKnown = true;
};

export default makePersonaKnown;