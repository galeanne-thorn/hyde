import { HeroSkills } from "./HeroSkills";
import { Money } from "./Money";
import { NpcState } from "./Npc";
import { PersonaState } from "./Persona";
import { PersonAttributes } from "./PersonAttributes";

/**
 * Hero persona state
 */
export interface HeroPersonaState extends PersonaState {
    /**
     * First name of hero as set by player
     */
    readonly firstName: string;
}

/**
 * Hero state
 */
export interface HeroState extends NpcState {
    /**
     * State of personas
     */
    readonly personas: Record<string, HeroPersonaState>;
    /**
     * Hero money
     */
    readonly wallet: Money;
    /**
     * Hero attributes
     */
    readonly attributes: PersonAttributes;
    /**
     * Hero skills
     */
    readonly skills: HeroSkills;
}
