import { Draft } from "immer";
import { GameState } from "../GameState";
import { HeroState } from "./HeroState";
import Npc from "./Npc";
import Persona from "./Persona";
import { attributeToExperience } from "./PersonAttributes";
import { defaultSkillExperience, Level } from "./SkillLevels";

// Unlike NPCs, hero's average is on fair level
// He is hero, after all ;)
const heroDefaultAttrExperience = attributeToExperience(Level.fair);

/**
 * Hero definition
 */
export class Hero extends Npc {

    /**
     * Creates new hero definition
     */
    public constructor() {
        super(
            "hero",
            new Persona(
                "male",
                "Anselm",
                {
                    firstName: "Adam",
                    title: "Mr."
                }
            ))
        this.withPersona(new Persona(
            "female",
            "Melville",
            {
                firstName: "Ada",
                title: "Ms."
            }
        ));
    }

    /**
     * Creates new hero state
     */
    public override createState(): HeroState {
        // Add main state
        const state: HeroState = {
            activePersona: this.defaultPersona.id,
            personas: {},
            location: "home-office",
            wallet: {
                pounds: 0,
                shillings: 0,
                pennies: 0
            },
            attributes: {
                faith: heroDefaultAttrExperience,
                fitness: heroDefaultAttrExperience,
                intelligence: heroDefaultAttrExperience,
                morale: heroDefaultAttrExperience,
                sexuality: heroDefaultAttrExperience,
                will: heroDefaultAttrExperience,
            },
            // Hero skills
            skills: {
                art: defaultSkillExperience,
                business: defaultSkillExperience,
                crime: defaultSkillExperience,
                mysticism: defaultSkillExperience,
                science: defaultSkillExperience,
                sex: defaultSkillExperience,
            },
        };

        // Add persona states
        this.personas.forEach((v, k) => {
            var personaState = {
                ...v.createState(),
                firstName: v.firstName!
            }
            state.personas[k] = personaState;
        });
        return state;
    }

    /**
     * Verifies that state exists for this stage
     * and creates new one if required
     * @param gameState - Game state draft to modify
     */
    public override getState(gameState: Draft<GameState>): Draft<HeroState> {
        // By default there is no state
        let state = gameState.hero;
        if (state === undefined) {
            throw new Error(`Hero state not found`);
        }
        return state;
    }

    /**
     * Gets the active persona for the hero
     * @param gameState - Current game state
     */
    public override getActivePersona(gameState: Draft<GameState>): Persona {
        const state = this.getState(gameState);
        const persona = this.personas.get(state.activePersona);
        if (persona === undefined) {
            throw new Error(`Hero does not have persona ${state.activePersona} !`);
        }
        return persona;
    }

}

export default Hero;