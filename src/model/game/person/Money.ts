/**
 * Number of pennies in shilling
 */
export const PennyPerShilling = 12;
/**
 * Number of shillings in pound
 */
export const ShillingPerPoint = 20;
/**
 * Number of pennies in pound
 */
export const PennyPerPound = ShillingPerPoint * PennyPerShilling;
/**
 * Number of pennies in guineas
 */
export const PennyPerGuinea = PennyPerPound + PennyPerShilling;

/**
 * Money wallet
 *
 * 1 Pound (£) = 20 Shillings (s) = 240 pennies (d)
 * 1 Shilling = 12 Pennies
 */
export interface Money {
    /**
     * Number of pennies
     */
    readonly pennies: number;
    /**
     * Number of shillings
     */
    readonly shillings: number;
    /**
     * Number of pounds
     */
    readonly pounds: number;
}

/**
 * Converts money to pennies
 * @param money
 * @return Value in pennies
 */
export function toPennies(money: Money): number {
    return (money.pounds * PennyPerPound)
        + (money.shillings * PennyPerShilling)
        + money.pennies;
}

/**
 * Converts pennies to money
 * @param pennies - Number of pennies
 * @return Money in pounds, shillings and pennies
 */
export function toMoney(pennies: number): Money {
    // Get whole number to prevent modulo on floats.
    // Always go down.
    const total = Math.floor(pennies);
    // Get pounds, shillings and pennies
    const pounds = Math.floor(total / PennyPerPound);
    let rest = total % PennyPerPound;
    const shillings = Math.floor(rest / PennyPerShilling);
    rest = rest % PennyPerShilling;
    return {
        pounds,
        shillings,
        pennies: rest
    };
}

/**
 * Adds money
 *
 * @param addTo
 * @param toAdd
 */
export function addMoney(value1: Money, value2: Money): Money {
    const total = toPennies(value1) + toPennies(value2);
    return toMoney(total);
}

/**
 * Adds (or subtracts if negative amount) pennies from wallet
 * @param wallet
 * @param pennies
 * @returns
 */
export function addPennies(wallet: Money, pennies: number): Money {
    return addMoney(wallet, { pounds: 0, shillings: 0, pennies });
}

/**
 * Adds (or subtracts if negative amount) shillings to wallet
 * @param wallet
 * @param pennies
 * @returns
 */
export function addShillings(wallet: Money, shillings: number): Money {
    return addMoney(wallet, { pounds: 0, shillings, pennies: 0 });
}

/**
 * Adds (or subtracts if negative amount) pounds to wallet
 * @param wallet
 * @param pennies
 * @returns
 */
export function addPounds(wallet: Money, pounds: number): Money {
    return addMoney(wallet, { pounds, shillings: 0, pennies: 0 });
}

/**
 * Adds (or subtracts if negative amount) pounds to wallet
 * @param wallet
 * @param pennies
 * @returns
 */
 export function addGuineas(wallet: Money, guineas: number): Money {
    const pennies = guineas * PennyPerGuinea;
    return addMoney(wallet, { pounds: 0, shillings: 0, pennies: pennies });
}
