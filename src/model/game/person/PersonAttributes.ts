import { Level } from "./SkillLevels";

/**
 * Person attributes
 *
 * Note attributes use same numeric range as skills (0..11)
 * but the levels have no names and are linear with
 * experience of 10 points per level (i.e. real range is 0..110)
 */
export interface PersonAttributes {
    /**
     * Faith
     * Skeptic / Dogmatic
     */
    readonly faith: number;
    /**
     * Will
     * Submissive / Dominant
     */
    readonly will: number;
    /**
     * Intelligence
     * Gullible / Cunning
     */
    readonly intelligence: number;
    /**
     * Morale
     * Cynical / Optimistic
     */
    readonly morale: number;
    /**
     * Sexuality
     * Pure / Corrupt
     */
    readonly sexuality: number;
    /**
     * Fitness
     * Weak / Strong
     */
    readonly fitness: number;
}

/**
 * Attribute experience per attribute level
 */
export const attributeExpPerLevel = 10;

/**
 * Converts experience to attribute level
 */
export function experienceToAttribute(experience: number): number {
    return Math.floor(experience / attributeExpPerLevel);
}

/**
 * Calculates minimal experience needed for given level
 */
export function attributeToExperience(level: number): number {
    return attributeExpPerLevel * level;
}

/**
 * Default attribute level
 */
export const defaultAttributeLevel = Level.average;

/**
 * Experience corresponding to default skill level
 */
export const defaultAttributeExperience = attributeToExperience(defaultAttributeLevel);

/**
 * Average attributes for NPCs
 */
export const AverageAttributes: PersonAttributes = {
    faith: defaultAttributeExperience,
    fitness: defaultAttributeExperience,
    intelligence: defaultAttributeExperience,
    morale: defaultAttributeExperience,
    sexuality: defaultAttributeExperience,
    will: defaultAttributeExperience,
};
