/**
 * Hero skills
 *
 * The number by the skill is the experience in that skill
 */
export interface HeroSkills {
    /**
     * Mysticism (aka esoteric knowledge)
     * Helps in finding the serum formula
     */
    readonly mysticism: number;
    /**
     * Science (aka hard knowledge)
     * Helps in finding the correct ingredients
     */
    readonly science: number;
    /**
     * Sexual skills
     * Helps with money making by prostitution
     */
    readonly sex: number;
    /**
     * Criminal skills
     * Helps with breaking and entering, pick-pocketing
     * and money making by scams
     */
    readonly crime: number;
    /**
     * Art
     * Helps in making clothes
     * and making money by art (singing, dancing)
     */
    readonly art: number;
    /**
     * Business
     * Helps in making money by legal operations
     */
    readonly business: number;
}
