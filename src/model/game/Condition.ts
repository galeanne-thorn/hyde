import { Game } from "./Game";
import { GameState } from "./GameState";

/**
 * Choice condition resolver
 *
 * @param state - Game state to use for evaluation
 * @param game - Game definition to use for evaluation
 */
export type Condition = (state: GameState, game: Game) => boolean;

/**
 * Always satisfied condition
 * @returns True
 */
export const always: Condition = () => true;