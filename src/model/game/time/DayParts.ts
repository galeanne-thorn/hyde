/**
 * Converts hour to day part
 * @param hour
 */
export function hourToDayPart(hour: number): string {
    // Normalize hour
    const h = hour % 24;
    // Convert to text
    if (h < 2) return "midnight";
    if (h < 5) return "early morning";
    if (h < 8) return "morning";
    if (h < 11) return "late morning";
    if (h < 14) return "noon"
    if (h < 17) return "afternoon";
    if (h < 20) return "evening";
    if (h < 20) return "night";
    return "midnight";
}