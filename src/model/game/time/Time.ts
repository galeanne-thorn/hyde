import { GameHandler } from "../GameHandler";

const minutePerDay = 1440;
const minutePerHour = 60;

/**
 * Time state
 */
export interface Time {
    /**
     * Days from game start
     */
    readonly day: number;
    /**
     * Hour
     */
    readonly hour: number;
    /**
     * Minute
     */
    readonly minute: number;
}

/**
 * Sets time.
 * Does not change day.
 */
export const setTime = (hour: number, minute: number = 0): GameHandler => s => {
    s.time.hour = hour;
    s.time.minute = minute;
}

/**
 * Adds time
 */
export const addTime = (hour: number, minute: number = 0): GameHandler => s => {
    // Get total minutes
    let time = (s.time.day * minutePerDay)
        + (s.time.hour + hour) * minutePerHour
        + s.time.minute + minute;
    // Split back to day-hour-minute
    const newDay = Math.floor(time / minutePerDay);
    time = time % minutePerDay;
    const newHour = Math.floor(time / minutePerHour);
    const newMinute = time % minutePerHour;

    s.time.day = newDay;
    s.time.hour = newHour;
    s.time.minute = newMinute;
}