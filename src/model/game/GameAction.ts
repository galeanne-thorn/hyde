import produce from "immer";
import game from "../../game/Game";
import { registerAction } from "../RootReducer";
import { getHandler } from "./GameHandler";

/**
 * Type discriminator for action 'Perform'
 */
export const ACTION_PERFORM = "PERFORM";

/**
 * Creates action to perform choice selection
 * @param handlerId - Id of the handler
 * @param data - Optional data
 */
export function perform(handlerId: string, data: any = undefined) {
    return {
        type: ACTION_PERFORM,
        handlerId,
        data
    };
}

/**
 * Game Action type
 */
export type GameAction = ReturnType<typeof perform>;

/**
 * Register the action
 */
registerAction(
    ACTION_PERFORM,
    (s, a: GameAction) => produce(s, n => {
        // Perform action
        getHandler(a.handlerId)(n.game, game, a.data);
        // Run events
        let events = game.getTriggeredEvents(n.game);
        events.forEach(e => e.handler(n.game, game));
    })
);
