import Choice from "../Choice";
import Stage, { STAGE_LAYER_BASE } from "../Stage";
import { RoomExit } from "./RoomExit";

/**
 * Type of room stages
 */
export const STAGE_ROOM = "ROOM";

/**
 * Room stage
 */
export class Room extends Stage {

    /**
     * Room exits
     */
    public readonly exits: Array<RoomExit> = [];

    /**
     * Choices combined from all exits
     */
    public readonly choices: Array<Choice> = [];

    /**
     * Creates new room
     *
     * @param id - Id of the room
     * @param name - Name of the room
     * @param description - Description of the room
     */
    public constructor(
        id: string,
        name: string,
        public description: string
    ) {
        super(STAGE_ROOM, STAGE_LAYER_BASE, id, name);
    }

    /**
     * Adds exit to the room
     * @param exit Exit to add
     * @returns This room
     */
    public withExit(exit: RoomExit): this {

        this.exits.push(exit);

        const exitId = this.exits.length;
        this.choices.push(new Choice(
            `${STAGE_ROOM}_${this.id}_${exitId}`,
            exit.description,
            exit.onEnter,
            (s, g) => (exit.visible?.(s, g) ?? true) && (exit.open?.(s, g) ?? true)
        ));
        if (exit.onLocked !== undefined) {
            this.choices.push(new Choice(
                `${STAGE_ROOM}_${this.id}_${exitId}_locked`,
                exit.description,
                exit.onLocked,
                (s, g) => (exit.visible?.(s, g) ?? true) && !(exit.open?.(s, g) ?? true)
            ));
        }
        return this;
    }
}