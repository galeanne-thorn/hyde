import { Condition } from "../Condition";
import { GameHandler } from "../GameHandler";

/**
 * Room exit
 *
 * Technically it is @see Choice without Id.
 * Id will be assigned by the Room
 */
export class RoomExit {
    /**
     * Condition that needs to evaluate to true
     * for the exist to be usable
     */
    public open: Condition | undefined;

    /**
     * Condition that needs to evaluate to true
     * for the exist to be visible
     */
    public visible: Condition | undefined;

    /**
     * Choice handler to perform when hero tries to use locked exist
     */
    public onLocked: GameHandler | undefined;

    /**
     * Creates new Room exit
     *
     * @param description - Description of the exist for UI
     * @param onEnter - Choice handler to perform when the exit is used
     */
    public constructor(
        public readonly description: string,
        public readonly onEnter: GameHandler
    ) {
    }

    /**
     * Sets exit open condition
     * @param condition - condition to set
     * @returns This exit
     */
    public openWhen(condition: Condition): this {
        if (this.open !== undefined) {
            throw new Error("Exit Open condition is already set");
        }
        this.open = condition;
        return this;
    }

    /**
     * Sets exit visible condition
     * @param condition - condition to set
     * @returns This exit
     */
    public visibleWhen(condition: Condition): this {
        if (this.visible !== undefined) {
            throw new Error("Exit Visible condition is already set");
        }
        this.visible = condition;
        return this;
    }

    /**
     * Sets optional handler for "locked" exit use
     * @param handler
     */
    public onLockedDo(handler: GameHandler) : this {
        if (this.onLocked !== undefined) {
            throw new Error("OnLocked handler is already set");
        }
        this.onLocked = handler;
        return this;
    }
}
