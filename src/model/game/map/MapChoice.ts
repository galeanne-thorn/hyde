import { Condition } from "../Condition";
import { GameHandler } from "../GameHandler";
import Choice from "../Choice";

/**
 * Choice on map
 */
export class MapChoice extends Choice {
    /**
     * Type of map choice
     */
    public override get type() { return "MAP"; }

    /**
     * Creates new text choice
     *
     * @param id - Id of the choice
     * @param handler - Choice handler
     * @param x - X location
     * @param y - Y location
     * @param icon - Icon to show
     * @param text - Choice alt text
     */
    constructor(
        id: string,
        public readonly x: number,
        public readonly y: number,
        public readonly icon: string,
        text: string,
        handler: GameHandler,
        condition: Condition | undefined = undefined
    ) {
        super(id, text, handler, condition);
    }
}

export default MapChoice;
