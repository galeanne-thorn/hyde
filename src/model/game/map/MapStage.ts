import { Condition } from "../Condition";
import { combine, GameHandler } from "../GameHandler";
import Stage, { STAGE_LAYER_DEFAULT } from "../Stage";
import MapChoice from "./MapChoice";

/**
 * Type of Map stage
 */
export const STAGE_MAP = "MAP";

/**
 * Map stage
 */
export class MapStage extends Stage {

    /**
     * Map choices
     */
    public readonly choices: Array<MapChoice> = [];

    /**
     * Creates new Map
     * @param id - Id of Map Stage
     * @param name - Name of the map
     * @param text - Text to show with the map
     * @param image - Name of the file with image.
     *      Expects image in /assets/map folder
     */
    public constructor(
        id: string,
        name: string,
        public readonly text: string,
        public readonly image: string
    ) {
        super(STAGE_MAP, STAGE_LAYER_DEFAULT, id, name);
    };

    /**
     * Choice to add to the scene
     * @param choice Choice to add
     * @returns This scene
     */
    public withLocation(
        x: number,
        y: number,
        icon: string,
        text: string,
        condition: Condition | undefined = undefined,
        handler: GameHandler,
        ...handlers: GameHandler[]): this {

        const choiceId = `${STAGE_MAP}_${this.id}_${x}_${y}`;
        this.choices.push(new MapChoice(
            choiceId,
            x, y,
            icon,
            text,
            combine(handler, ...handlers),
            condition
        ));
        return this;
    }
}

export default MapStage;
