import { Condition } from "./Condition";
import { combine, GameHandler } from "./GameHandler";

/**
 * Game event.
 *
 * Game event is evaluated on every player's choice
 * and if it should be triggered, it runs its handler.
 *
 * Note that state changes induced by events do not trigger
 * other events until next player choice.
 */
export class GameEvent implements GameEvent {
    /**
     * Handler for the event
     */
    public readonly handler: GameHandler;

    /**
     * Creates new game event
     * @param - Id of the event for debugging purposes
     * @param condition
     * @param handler
     */
    public constructor(
        public readonly id: string,
        public readonly condition: Condition,
        handler: GameHandler,
        ...handlers: GameHandler[]
    ) {
        this.handler = combine(handler, ...handlers);
    }
}