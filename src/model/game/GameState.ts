import { SceneState } from "./scene";
import { NpcState, HeroState } from "./person";
import { QuestState } from "./quest";
import { Time } from "./time";

/**
 * State of the game
 */
export interface GameState {
    /**
     * Stack of active stages
     * stages[0] is the current stage
     */
    readonly stages: Array<string>;
    /**
     * Scene Stage states
     */
    readonly scenes: Record<string, SceneState>;
    /**
     * Hero state data
     */
    readonly hero: HeroState;
    /**
     * NPC state data
     */
    readonly npcs: Record<string, NpcState>;
    /**
     * Quests state data
     */
    readonly quests: Record<string, QuestState>;
    /**
     * Time
     */
    readonly time: Time;
}
