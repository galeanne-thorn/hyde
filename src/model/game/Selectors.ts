import { useApp } from "../Selectors";
import { GameState } from "./GameState";

/**
 * Typed game state selector
 */
export type GameStateSelector<T> = (state: GameState) => T;

/**
 * Game state hook
 */
export function useGameState<T>(selector: GameStateSelector<T>): T {
    return useApp(state => selector(state.game)) as T;
}
