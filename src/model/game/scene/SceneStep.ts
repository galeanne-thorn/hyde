import Choice from "../Choice";
import { Condition } from "../Condition";
import { Game } from "../Game";
import { combine, GameHandler } from "../GameHandler";
import { GameState } from "../GameState";

/**
 * Base class for scene steps
 */
export abstract class SceneStep {

    /**
     * Step condition
     */
    public condition: Condition | undefined;

    /**
     * Step display action
     */
    public onDisplay: GameHandler | undefined;

    /**
     *
     * @param type - Type of the step
     * @param choices - (Optional) Choices to be made after this scene step.
     *      If empty, the scene will progress to next step automatically
     */
    protected constructor(
        public readonly type: string,
        public choices: Array<Choice> | undefined = undefined
    ) { }

    /**
     * Gets whether step can be used (shown) based on its condition
     * @param state - Current game state
     * @param game - Game definition
     * @returns True if state can be used
     */
    public canUse(state: GameState, game: Game): boolean {
        if (this.condition === undefined) return true;
        return this.condition(state, game);
    }

    /**
     * Add handler to run when the step is displayed
     * @param handler
     * @returns
     */
    public onDisplayDo(handler: GameHandler, ...handlers: GameHandler[]): this {
        this.onDisplay = combine(handler, ...handlers);
        return this;
    }

    /**
     * Adds choice to
     * @param choice Choice to add
     * @returns
     */
    public addChoice(choice: Choice) : this {
        if (this.choices === undefined) {
            this.choices = [choice];
        }
        else {
            this.choices.push(choice);
        }
        return this;
    }

    /**
     * Adds condition to the step
     * @param condition
     */
    public addCondition(condition: Condition) : this {
        this.condition = condition;
        return this;
    }
}

export default SceneStep;
