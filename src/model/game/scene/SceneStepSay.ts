import Choice from "../Choice";
import SceneStep from "./SceneStep";

/**
 * Type of scene step Say
 */
export const SCENE_STEP_SAY = "SAY";

/**
 * Simple "talk" scene
 */
export class SceneStepSay extends SceneStep {
    /**
     * Creates new "say" scene step
     *
     * @param who - Id of the person talking
     * @param text - Text to be said
     * @param choices - Choices at the end of the scene step
     */
    public constructor(
        public readonly who: string,
        public readonly text: string,
        choices: Array<Choice> | undefined = undefined
    ) {
        super(SCENE_STEP_SAY, choices);
    }
}

export default SceneStepSay;
