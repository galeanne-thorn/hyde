import { Condition } from "../Condition";
import { combine, GameHandler } from "../GameHandler";

/**
 * Scene choice definition.
 *
 * Technically it is @see TextChoice without Id.
 * Id will be assigned in the Scene
 */
export class SceneChoice {

    /**
     * Choice handler
     */
    public readonly handler: GameHandler;

    /**
     * Condition
     */
    public condition: Condition | undefined;

    /**
     * Creates new Scene Choice
     *
     * @param reply
     * @param handlers
     */
    public constructor(
        public readonly reply: string,
        handler: GameHandler,
        ...handlers: GameHandler[]
    ) {
        this.handler = combine(handler, ...handlers);
    }

    /**
     * Adds condition to the choice
     * @param condition
     * @returns
     */
    public onlyWhen(condition: Condition): this {
        this.condition = condition;
        return this;
    }
}

export default SceneChoice;
