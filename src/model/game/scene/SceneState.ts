/**
 * State of the scene step
 */
export interface SceneStepState {
}

/**
 * State of the scene
 */
export interface SceneState {
    /**
     * Current step
     */
    readonly currentStep: number;
    /**
     * Indexes of already passed scene steps
     */
    readonly doneSteps: Array<number>;
}
