import { Draft } from "immer";
import { GameState } from "../GameState";
import SceneStep from "./SceneStep";

/**
 * Type of scene step Input
 */
export const SCENE_STEP_INPUT = "INPUT";

/**
 * Input callback
 */
export type SceneInputCallback = (state: Draft<GameState>, value: string) => void;

/**
 * Player has to type in hero's answer
 */
export class SceneStepInput extends SceneStep {
    /**
     * Creates new scene step for input
     *
     * @param text - Text to be shown before input
     * @param defaultValue - Default value
     * @param onInput - Callback when input is provided
     */
    public constructor(
        public readonly text: string,
        public readonly defaultValue: string,
        public readonly handlerId: string
    ) {
        super(SCENE_STEP_INPUT);
    }
}

export default SceneStepInput;
