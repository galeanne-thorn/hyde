import { Draft } from "immer";
import Choice, { WithChoices } from "../Choice";
import { Condition } from "../Condition";
import { Game } from "../Game";
import { addHandler, combine, GameHandler } from "../GameHandler";
import { GameState } from "../GameState";
import Stage, { STAGE_LAYER_DEFAULT } from "../Stage";
import { advanceScene } from "./continueScene";
import SceneChoice from "./SceneChoice";
import { SceneState } from "./SceneState";
import SceneStep from "./SceneStep";
import SceneStepInput, { SceneInputCallback } from "./SceneStepInput";
import SceneStepNarration from "./SceneStepNarration";
import SceneStepReply from "./SceneStepReply";
import SceneStepSay from "./SceneStepSay";

/**
 * Type of Scene stage
 */
export const STAGE_SCENE = "SCENE";

/**
 * Scene stage
 */
export class Scene extends Stage implements WithChoices {

    /**
     * Scene steps
     */
    public readonly steps: SceneStep[] = [];

    /**
     * Scene step labels
     */
    public readonly labels = new Map<string, number>();

    /**
     * Choices combined from all steps
     */
    public readonly choices: Array<Choice> = [];

    /**
     * Creates new scene stage
     * @param id
     * @param name
     */
    public constructor(
        id: string,
        name: string
    ) {
        super(STAGE_SCENE, STAGE_LAYER_DEFAULT, id, name)
    }

    /**
     * Finds index of next usable step
     *
     * @param startWith Id from which to start (inclusive)
     */
    public getNextStepId(state: GameState, game: Game, startWith: number = 0) {
        let nextStep = startWith;
        // Move according to condition
        while (nextStep < this.steps.length) {
            if (this.steps[nextStep].canUse(state, game)) break;
            nextStep++;
        }
        if (nextStep === this.steps.length) {
            throw new Error(`No usable step for Scene ${this.id}!`);
        }
        return nextStep;
    }

    /**
     * Verifies that state exists for this stage
     * and creates new one if required
     * @param game - Game definition
     * @param state - Game state draft to modify
     */
    public override getOrCreateState(state: Draft<GameState>, game: Game): Draft<SceneState> {
        let sceneState = state.scenes[this.id];
        if (sceneState === undefined) {
            // Get first usable step
            let currentStep = this.getNextStepId(state, game);
            // Create state
            sceneState = {
                currentStep,
                doneSteps: []
            };
            state.scenes[this.id] = sceneState;
        }
        return sceneState;
    }

    /**
     * Marks next step with label with given Id
     * @param label Label Id
     * @returns This scene
     */
    public nextStepLabel(label: string): this {
        if (this.labels.has(label)) {
            throw new Error(`Label ${label} is already defined for Dialog ${this.id}`);
        }
        this.labels.set(label, this.steps.length);
        return this
    }

    /**
     * Adds scene step with NPC talking
     *
     * @param who - Who is talking
     * @param text - What is being said
     * @returns This scene
     */
    public narrate(text: string): this {
        this.steps.push(new SceneStepNarration(
            text
        ));

        return this;
    }

    /**
     * Adds scene step with NPC talking
     *
     * @param who - Who is talking
     * @param text - What is being said
     * @returns This scene
     */
    public say(who: string, text: string): this {
        // Create step
        this.steps.push(new SceneStepSay(
            who,
            text
        ));

        return this;
    }

    /**
     * Adds scene step with hero's automatic reply
     *
     * @param text - Reply text
     * @returns This scene
     */
    public reply(text: string): this {
        this.steps.push(new SceneStepReply(
            text
        ));
        return this;
    }

    /**
     * Adds scene step where player has to input information
     *
     * @param text - Text to show before input
     * @param defaultValue - Default value to use
     * @param callback - Callback to call when input is provided
     * @returns This scene
     */
    public input(text: string, defaultValue: string, callback: SceneInputCallback): this {
        const handlerId = `__INPUT_${this.id}_${this.steps.length}`;
        this.steps.push(new SceneStepInput(
            text,
            defaultValue,
            handlerId
        ));
        addHandler(
            handlerId,
            combine(
                (s, g, d) => (callback(s, d as string)),
                advanceScene
            )
        );
        return this;
    }

    /**
     * Adds condition to the last step of the scene
     * @param choices
     */
    public when(condition: Condition): this {
        const { lastStep } = this.getLastStep();
        lastStep.addCondition(condition);
        return this;
    }

    /**
     * Adds scene choice to the last step of the scene
     * @param text - Choice text
     * @param handler - Choice handler
     * @param condition - Optional choice condition
     */
    public withChoice(
        text: string,
        handler: GameHandler,
        condition?: Condition
    ): this {
        const { lastStep, stepId } = this.getLastStep();
        let choiceId = lastStep.choices?.length ?? 0;
        const choice = new Choice(
            `${STAGE_SCENE}_${this.id}_${stepId}_${choiceId}`,
            text,
            handler,
            condition
        );
        lastStep.addChoice(choice);
        this.choices.push(choice)
        return this;
    }

    /**
     * Adds choices to the last step of the scene
     * @param choices
     */
    public withChoices(...stepChoices: SceneChoice[]): this {

        const { lastStep, stepId } = this.getLastStep();
        let choiceId = lastStep.choices?.length ?? 0;

        stepChoices
            .map((ch, i) => new Choice(
                `${STAGE_SCENE}_${this.id}_${stepId}_${choiceId + i}`,
                ch.reply,
                ch.handler,
                ch.condition
            ))
            .forEach(ch => {
                lastStep.addChoice(ch);
                this.choices.push(ch);
            });

        return this;
    }

    /**
     * Add handler to display of last step
     * @param handler
     * @param handlers
     */
    public onStepDo(handler: GameHandler, ...handlers: GameHandler[]): this {
        const { lastStep } = this.getLastStep();
        lastStep.onDisplayDo(handler, ...handlers);
        return this;
    }

    private getLastStep() {
        const stepId = this.steps.length - 1;
        const lastStep = this.steps[stepId];
        return { lastStep, stepId };
    }
}

export default Scene;