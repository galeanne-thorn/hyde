import Choice from "../Choice";
import SceneStep from "./SceneStep";

/**
 * Type of scene step Reply
 */
 export const SCENE_STEP_REPLY = "REPLY";

/**
 * Hero answer in the scene without choices
 */
export class SceneStepReply extends SceneStep {
    /**
     * Creates new reply scene step
     *
     * @param text - Text to be said
     */
    public constructor(
        public readonly text: string,
        choices: Array<Choice> | undefined = undefined
    ) {
        super(SCENE_STEP_REPLY, choices);
    }
}

export default SceneStepReply;
