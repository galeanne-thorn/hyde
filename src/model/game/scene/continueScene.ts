import { addHandler, GameHandler } from "../GameHandler";
import Scene from "./SceneStage";

/**
 * Goes forward in scene
 * @param label - Scene step label. If not provided, goes to next step
 * @returns
 */
export const continueScene = (label: string | undefined = undefined): GameHandler => (n, g) => {
    const stageId = n.stages[0];
    const scene = g.getStage<Scene>(stageId);

    // Get scene state or get new one
    const sceneState = scene.getOrCreateState(n, g);

    // Mark current step as done
    sceneState.doneSteps.push(sceneState.currentStep);

    // Set new current step
    let nextStep: number | undefined;
    if (label === undefined) {
        nextStep = sceneState.currentStep + 1;
    }
    else {
        nextStep = scene.labels.get(label);
        if (nextStep === undefined) {
            throw new Error(`Label ${label} not defined in scene ${stageId}`);
        }
    }
    // Move according to condition
    nextStep = scene.getNextStepId(n, g, nextStep);
    // Update state
    sceneState.currentStep = nextStep;
    n.scenes[stageId] = sceneState;
    // Perform step action
    scene.steps[nextStep].onDisplay?.(n, g);
};

export default continueScene;

/**
 * Pre-compiled handler to advance scene by one step
 */
export const advanceScene = continueScene();

/**
 * Id of the advanceScene handler
 */
export const ADVANCE_SCENE = "__advance_scene";

// Register the handler
addHandler(ADVANCE_SCENE, advanceScene);
