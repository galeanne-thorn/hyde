import Choice from "../Choice";
import SceneStep from "./SceneStep";

/**
 * Type of scene step Narration
 */
export const SCENE_STEP_NARRATION = "NARRATION";

/**
 * Narration part of the scene
 */
export class SceneStepNarration extends SceneStep {
    /**
     * Creates new "say" scene step
     *
     * @param text - Text to be shown
     * @param choices - Optional choices at the end of the scene step
     */
    public constructor(
        public readonly text: string,
        choices: Array<Choice> | undefined = undefined
    ) {
        super(SCENE_STEP_NARRATION, choices);
    }
}

export default SceneStepNarration;
