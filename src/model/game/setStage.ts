import { GameHandler } from "./GameHandler";

/**
 * Creates choice handler that sets stage as active
 * @param newStageId - Id of the stage to set
 * @returns
 */
export const setStage = (newStageId: string): GameHandler => (s, g) => {

    // Stage is already active - nothing to do
    if (newStageId === s.stages[0]) return;

    // Get the new stage
    const newStage = g.getStage(newStageId);

    // Helper function to get current stage
    const getCurrentStage = () => s.stages[0]
        ? g.getStage(s.stages[0])
        : undefined;

    // Get current stage Id
    let currentStage = getCurrentStage();

    // If layers are same, replace the stage.
    // If layer is higher, put it to top of stack.
    // If layer is lower, remove higher layers first.
    while (currentStage != undefined && currentStage.layer > newStage.layer) {
        s.stages.shift();
        currentStage = getCurrentStage();
    }
    if (currentStage === undefined) {
        s.stages = [newStageId];
    }
    else if (newStage.layer > currentStage.layer) {
        s.stages.unshift(newStageId);
    }
    else if (newStage.layer === currentStage.layer) {
        s.stages.splice(0, 1, newStageId);
    }
    // Create stage state if necessary
    newStage.getOrCreateState(s, g);
    // Call action if defined
    newStage.onEntry?.(s, g);
};

export default setStage;
