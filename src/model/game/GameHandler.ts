import { Draft } from "immer";
import { Game } from "./Game";
import { GameState } from "./GameState";

/**
 * Game sta handler
 */
export type GameHandler = (state: Draft<GameState>, game: Game, data?: unknown) => void

/**
 * Combines state handlers into one
 * @param params
 */
export function combine(handler: GameHandler, ...handlers: GameHandler[]): GameHandler {
    if (handlers.length === 0) return handler;
    return (s, g, d) => [handler, ...handlers].forEach(h => h(s, g, d));
}

/**
 * Game handlers registry
 */
const handlers: Map<string, GameHandler> = new Map();

/**
 * Registers handler
 * @param choice - Handler add
 */
export function addHandler(id: string, handler: GameHandler): void {
    if (handlers.has(id)) {
        throw new Error(`Handler Id '${id}' already exists`);
    }
    handlers.set(id, handler);
}

/**
 * Gets handler
 */
export function getHandler(id: string): GameHandler {
    const result = handlers.get(id);
    if (result === undefined) {
        throw new Error(`Handler with Id '${id}' not found`);
    }
    return result;
}
