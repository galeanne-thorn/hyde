import { GameHandler } from "./GameHandler";
import { Stage, STAGE_LAYER_UI } from "./Stage";

export const STAGE_UI = "UI";

/**
 * Generic UI stage
 */
export class UiStage extends Stage {

    /**
     * Creates new UI stage
     * @param id
     * @param name
     */
    public constructor(
        id: string,
        name: string
    ) {
        super(STAGE_UI, STAGE_LAYER_UI, id, name)
    }
}
