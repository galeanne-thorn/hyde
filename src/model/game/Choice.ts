import { Condition } from "./Condition";
import { addHandler, GameHandler } from "./GameHandler";

/**
 * Choice class
 */
export class Choice {
    /**
     * Choice type
     */
    public get type(): string { return "PLAIN" };

    /**
     * Creates new choice
     * @param text - Choice text
     * @param id -  Id of the choice
     * @param handler - Callback to modify game state on choice selection
     * @param condition - Choice condition
     */
    public constructor(
        public readonly id: string,
        public readonly text: string,
        handler: GameHandler,
        public readonly condition: Condition | undefined = undefined
    ) {
        // Register handler under the choice Id
        addHandler(id, handler);
    }
}

/**
 * Interface for game object with choices
 */
export interface WithChoices {
    /**
     * List of choices
     */
    readonly choices: Array<Choice>;
}

export default Choice;