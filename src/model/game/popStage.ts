import { addHandler, GameHandler } from "./GameHandler";

/**
 * Handler that pops current stage and returns to stage one level up.
 *
 * Does nothing if there is only one stage in the stack
 * @param newStageId
 * @returns
 */
export const popStage: GameHandler = n => {
    if (n.stages.length <= 1) return;
    n.stages.shift();
}

export default popStage;

/**
 * Id of the default popStage handler
 */
export const POP_STAGE = "_popStage";

// Register the handler
addHandler(POP_STAGE, popStage);
