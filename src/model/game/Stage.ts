import { Draft } from "immer";
import { Game } from "./Game";
import { combine, GameHandler } from "./GameHandler";
import { GameState } from "./GameState";

/**
 * Lowest stage layer, usually for rooms
 */
export const STAGE_LAYER_BASE = 0;
/**
 * Default stage layer, usually for scenes
 */
export const STAGE_LAYER_DEFAULT = 100;
/**
 * UI stages layer
 */
export const STAGE_LAYER_UI = 1000;

/**
 * Scene class
 */
export abstract class Stage implements Stage {

    public onEntry?: GameHandler;

    /**
     * Creates new Scene
     * @param type Type of the stage
     * @param layer Layer to place the stage on.
     *      This is used to determine the viewing precedence
     * @param id Stage Id
     * @param name Name of the stage
     *      Used for example in saves
     */
    protected constructor(
        public readonly type: string,
        public readonly layer: number,
        public readonly id: string,
        public readonly name: string,
    ) { };

    /**
     * Adds action that will be performed when scene starts
     * @param handler Handler to perform on scene start
     */
    public onEntryDo(handler: GameHandler, ...handlers: GameHandler[]): this {
        this.onEntry = combine(handler, ...handlers);
        return this;
    }

    /**
     * Verifies that state exists for this stage
     * and creates new one if required
     *
     * @param game - Game definition
     * @param state - Game state draft to modify
     */
    public getOrCreateState(state: Draft<GameState>, game: Game): unknown {
        // By default there is no state
        return undefined;
    }
}

export default Stage;
