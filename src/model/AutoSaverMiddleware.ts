import { Middleware } from "redux";
import game from "../game/Game";
import persister from "../persistence/GamePersister";
import { ADVANCE_SCENE, STAGE_UI } from "./game";
import { ACTION_PERFORM } from "./game/GameAction";

/**
 * Redux middleware for automatic saving of game state
 */
export const autoSaver: Middleware = store => next => action => {
    // Autosave on Choose action
    if (action.type === ACTION_PERFORM) {
        const handlerId = action.handlerId;
        if (handlerId != ADVANCE_SCENE) {
            const oldState = store.getState().game;
            const stage = game.getStage(oldState.stages[0]);
            if (stage.type !== STAGE_UI) {
                persister.autoSave(oldState, stage.name);
            }
        }
    }
    return next(action);
}

export default autoSaver;
