import { AnyAction } from "redux";
import { AppState, initialState } from "./AppState";

/**
 * App reducer
 */
interface ActionHandler<TAction extends AnyAction = AnyAction> {
    (state: AppState, action: TAction): AppState;
};

/**
 * Action reducers dictionary
 */
const handlers: Map<string, ActionHandler> = new Map();

/**
 * Registers action handler to reducer
 * @param type
 * @param handler
 */
export function registerAction<TAction extends AnyAction>(
    type: string,
    handler: ActionHandler<TAction>
): void {

    if (handlers.has(type)) {
        throw new Error(`Handler for action ${type} is already registered`);
    }

    handlers.set(type, handler as ActionHandler);
}

/**
 * Main reducer
 * @param state
 * @param action
 */
export default function rootReducer(state: AppState = initialState, action: AnyAction): AppState {
    var handler = handlers.get(action.type);
    if (handler === undefined) {
        return state;
    }
    return handler(state, action);
}