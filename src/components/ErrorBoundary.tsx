import { Alert, AlertTitle } from "@material-ui/lab";
import React, { Component, ErrorInfo, ReactNode } from "react";

/**
 * Error boundary properties
 */
interface ErrorBoundaryProps {
    children: ReactNode;
}

/**
 * Error boundary state
 */
interface State {
    /**
     * Indicates there is an error
     */
    hasError: boolean;
    /**
     * Error description
     */
    error?: Error;
}

/**
 * Error boundary
 */
class ErrorBoundary extends Component<ErrorBoundaryProps, State> {

    public override state: State = {
        hasError: false,
        error: undefined,
    };

    /**
     * Update state when error is caught
     * @param error
     * @returns New state
     */
    public static getDerivedStateFromError(error: Error): State {
        return {
            hasError: true,
            error
        };
    }

    /**
     * Component did catch an error
     * @param error
     * @param errorInfo
     */
    public override componentDidCatch(error: Error, errorInfo: ErrorInfo) {
        // Log error to console
        console.error("Uncaught error:", error, errorInfo);
    }

    /**
     * Renders the component
     * @returns
     */
    public override render() {
        // If there is an error, render error information
        if (this.state.hasError) {
            return (
                <Alert severity="error" style={{margin: "1em"}}>
                    <AlertTitle>Unexpected Error</AlertTitle>
                    We are sorry, but there was an unexpected error.
                    <p>
                        {`${this.state.error}`}
                    </p>
                </Alert>
            );
        }
        // Otherwise render children
        return this.props.children;
    }
}

/**
 * Error boundary
 */
export default ErrorBoundary;