import React from "react";
import { useDispatch } from "react-redux";
import { useStyles } from "./MainMenu";
import MainMenuButton from "./MainMenuButton";
import aboutText from "../../game/About.md";
import MarkdownText from "../MarkdownText";

/**
 * About Menu
 */
export default function AboutMenu() {

    const dispatch = useDispatch();
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <MarkdownText template={aboutText} />
            <MainMenuButton />
        </div>
    );
}