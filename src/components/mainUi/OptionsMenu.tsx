import { Paper, Slider, Typography } from "@material-ui/core";
import React from "react";
import options, { saveOptions } from "../../Options";
import { useStyles } from "./MainMenu";
import MainMenuButton from "./MainMenuButton";

const stepDelayMarks = [
    { value: 0, label: "Off" },
    { value: 500, label: "Fast" },
    { value: 1000, label: "Default" },
    { value: 2000, label: "Slow" },
];
/**
 * Options Menu
 */
export default function OptionsMenu() {

    const classes = useStyles();

    const [stepDelay, setStepDelay] = React.useState(options.stepDelay);
    const updateStepDelay = (e: React.ChangeEvent<{}>, value: number | number[]) => {
        options.stepDelay = value as number;
        saveOptions();
        setStepDelay(value as number);
    }

    return (
        <div className={classes.root}>
            <Typography variant="h4" align="center" component="h2">
                Options
            </Typography>
            <Paper className={classes.block}>
                <Typography gutterBottom>
                    Auto-progress delay
                </Typography>
                <Slider
                    style={{width:"80%"}}
                    color="secondary"
                    valueLabelDisplay="auto"
                    marks={stepDelayMarks}
                    step={null}
                    min={0}
                    max={2000}
                    value={stepDelay}
                    onChange={updateStepDelay}
                />
            </Paper>
            <MainMenuButton />
        </div>
    );
}