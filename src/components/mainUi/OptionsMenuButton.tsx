import SettingsIcon from '@material-ui/icons/Settings';
import React from "react";
import { useDispatch } from "react-redux";
import setMode from '../../model/mainUi/setModeAction';
import { UiMode } from '../../model/mainUi/UiState';
import MenuButton from "./MenuButton";

/**
 * Options menu button
 * @param props
 * @returns
 */
export default function OptionsMenuButton() {

    const dispatch = useDispatch();

    return (
        <MenuButton
            startIcon={<SettingsIcon />}
            onClick={() => dispatch(setMode(UiMode.OptionsMenu))}
        >
            Options
        </MenuButton>
    );
}
