import { Button, ButtonProps, createStyles, makeStyles, Theme } from "@material-ui/core";
import React from "react";

/**
 * Properties for main menu button
 */
export type MainMenuButtonProps = Omit<ButtonProps, "size" | "variant" | "fullWidth">;

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            margin: theme.spacing(1),
        },
    }),
);

/**
 * Main menu button
 *
 * @param props
 * @returns
 */
export default function MenuButton(props: MainMenuButtonProps) {

    const classes = useStyles();

    const finalProps : ButtonProps = {
        ...props,
        size: "large",
        variant: "contained",
        fullWidth: true,
    }

    return (
        <Button {...finalProps} className={classes.root} />
    );
}