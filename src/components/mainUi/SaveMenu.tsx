import { createStyles, GridList, GridListTile, makeStyles, Theme, Typography } from "@material-ui/core";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Game, useGameState } from "../../model/game";
import loadGame from "../../model/loadGameAction";
import { useUi } from "../../model/mainUi/Selectors";
import setMode from "../../model/mainUi/setModeAction";
import { UiMode } from "../../model/mainUi/UiState";
import persister from "../../persistence/GamePersister";
import ContinueGameButton from "./ContinueGameButton";
import { useStyles } from "./MainMenu";
import MainMenuButton from "./MainMenuButton";
import SaveSlot from "./SaveSlot";
import SavingSlot from "./SavingSlot";

export const useLocalStyles = makeStyles((theme: Theme) =>
    createStyles({
        slots: {
            margin: 0,
            width: "100%",
        },
    }),
);

/**
 * Save menu properties
 */
export interface SaveMenuProps {
    /**
     * Game to show
     */
    game: Game;
}

/**
 * Save Menu
 */
export default function SaveMenu(props: SaveMenuProps) {

    const classes = useStyles();
    const localClasses = useLocalStyles();
    const dispatch = useDispatch();

    const gameState = useGameState(s => s);

    const [savedSlots, setSlots] = useState(persister.saveSlots);

    const gameInProgress = useUi(s => s.gameInProgress);

    const slots = savedSlots.slots;

    // Sort by date descending
    slots.sort((a, b) => (a.date > b.date) ? -1 : 1);

    const slotTiles = slots.map(s =>
        <GridListTile key={s.id}>
            <SaveSlot
                slot={s}
                onLoad={id => dispatch(loadGame(id))}
                onDelete={id => {
                    persister.clear(id);
                    setSlots(persister.saveSlots);
                }}
            />
        </GridListTile>
    );
    let continueButton = null;
    if (gameInProgress) {
        const description = props.game.getStage(gameState.stages[0]).name;
        slotTiles.unshift(
            <SavingSlot
                key={"new-save"}
                description={description}
                onSave={d => {
                    persister.save(gameState, d);
                    dispatch(setMode(UiMode.Game))
                }}
            />);
        continueButton = <ContinueGameButton
            gameInProgress={gameInProgress}
            saveExists={slots.length > 0}
        />;
    }

    return (
        <div className={classes.root}>
            <Typography variant="h4" align="center" component="h2">
                Saved Games
            </Typography>
            <hr />
            {continueButton}
            <MainMenuButton />
            <hr />
            <GridList cellHeight="auto" className={localClasses.slots}>
                {slotTiles}
            </GridList>
        </div>
    );
}
