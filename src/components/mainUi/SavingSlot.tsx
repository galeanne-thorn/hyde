import { Card, CardActions, CardContent, createStyles, IconButton, makeStyles, TextField, Theme } from "@material-ui/core";
import SaveIcon from '@material-ui/icons/Save';
import React, { useState } from "react";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: "100%",
            marginBottom: theme.spacing(2),
            backgroundColor: theme.palette.primary.main,
        },
        field: {
            backgroundColor: theme.palette.primary.light,
        },
        loadButton: {
            marginLeft: "auto",
            backgroundColor: theme.palette.primary.light,
        }
    }),
);

/**
 * Saving slot properties
 */
export interface SavingSlotProps {
    /**
     * Default save description
     */
    description: string;
    /**
     * Callback on save
     */
    onSave(description: string): void;
}

/**
 * Save Slot component
 * @param props
 * @returns
 */
export default function SavingSlot(props: SavingSlotProps) {

    const classes = useStyles();

    const [description, setDescription] = useState(props.description);

    const error = description.trim() === "";

    return (
        <Card className={classes.root}>
            <CardContent>
                <TextField
                    autoFocus
                    className={classes.field}
                    error={error}
                    value={description}
                    helperText="Please provide description for the save"
                    onChange={(e) => setDescription(e.target.value)}
                    size="small"
                    variant="outlined"
                    fullWidth
                    required
                />
            </CardContent>
            <CardActions disableSpacing>
                <IconButton
                    disabled={error}
                    color="secondary"
                    className={classes.loadButton}
                    onClick={() => props.onSave(description)}
                >
                    <SaveIcon />
                </IconButton>
            </CardActions>
        </Card>
    );
}
