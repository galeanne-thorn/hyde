import React from "react";
import startNewGame from "../../model/startNewGameAction";
import ReplayIcon from '@material-ui/icons/Replay';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import MenuButton from "./MenuButton";
import { useDispatch } from "react-redux";

/**
 * Properties for NewGameButton
 */
export interface NewGameButtonProps {
    /**
     * Is game in progress?
     */
    gameInProgress: boolean;
}

/**
 * Main menu New Game Button
 * @param props
 */
export default function NewGameButton(props: NewGameButtonProps) {

    const dispatch = useDispatch();

    return (
        <MenuButton
            color={
                props.gameInProgress
                    ? undefined
                    : "primary"
            }
            startIcon={
                props.gameInProgress
                    ? <ReplayIcon />
                    : <PlayArrowIcon />
            }
            onClick={() => dispatch(startNewGame())}
        >
            New Game
        </MenuButton>
    );
}
