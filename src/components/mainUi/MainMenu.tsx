import { Box, createStyles, makeStyles, Theme, Typography } from "@material-ui/core";
import React from "react";
import { useUi } from "../../model/mainUi/Selectors";
import AboutMenuButton from "./AboutMenuButton";
import ContinueGameButton from "./ContinueGameButton";
import SaveMenuButton from "./SavedGamesMenuButton";
import NewGameButton from "./NewGameButton";
import OptionsMenuButton from "./OptionsMenuButton";
import persister from "../../persistence/GamePersister";

export const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: "flex",
            flexFlow: "column",
            justifyContent: "center",
            alignItems: "center",
            margin: "auto",
            marginTop: theme.spacing(6)
        },
        block: {
            width: "100%",
            padding: theme.spacing(1),
            textAlign: "center",
        }
    }),
);

/**
 * Main menu screen
 * @returns
 */
export default function MainMenu() {

    const classes = useStyles();
    const saveExists = persister.anySaveExists;
    const gameInProgress = useUi(s => s.gameInProgress);

    return (
        <div className={classes.root}>
            <Typography variant="subtitle1">
                a transformation game by Galeanne Thorn
            </Typography>
            <Box margin="1em">
                <Typography variant="h4" align="center" component="h2">
                    DISCLAIMER
                </Typography>
                <Typography variant="body1">
                    This is game intended for adult audience.
                    If you are not of a legal age or you are offended by the game themes,
                    please do not play the game.
                </Typography>
            </Box>
            <ContinueGameButton
                saveExists={saveExists}
                gameInProgress={gameInProgress}
            />
            <NewGameButton
                gameInProgress={gameInProgress}
            />
            <SaveMenuButton
                saveExists={saveExists}
                gameInProgress={gameInProgress}
            />
            <OptionsMenuButton />
            <AboutMenuButton />
        </div>
    );
}