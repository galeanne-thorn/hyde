import { Card, CardActions, CardContent, createStyles, IconButton, makeStyles, Theme, Typography } from "@material-ui/core";
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import React from "react";
import { SlotDescription } from "../../persistence/Persister";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        loadButton: {
            marginLeft: "auto",
            backgroundColor: theme.palette.primary.main,
        },
        deleteButton: {
            color: theme.palette.error.light,
        }
    }),
);

/**
 * Save Slot properties
 */
 export interface SaveSlotProps {
    /**
     * Description of the save slot
     */
    slot: SlotDescription
    /**
     * On delete callback
     */
    onDelete(id: string): void;
    /**
     * On load callback
     */
    onLoad(id: string): void;
}

/**
 * Save Slot component
 * @param props
 * @returns
 */
export default function SaveSlot(props: SaveSlotProps) {

    const classes = useStyles();

    return (
        <Card>
            <CardContent>
                <Typography variant="subtitle2">
                    {props.slot.name}
                </Typography>
                {props.slot.date.toLocaleString()}
            </CardContent>
            <CardActions disableSpacing>
                <IconButton
                    size="small"
                    className={classes.deleteButton}
                    onClick={() => props.onDelete(props.slot.id)}
                    >
                    <DeleteForeverIcon />
                </IconButton>
                <IconButton
                    color="secondary"
                    className={classes.loadButton}
                    onClick={() => props.onLoad(props.slot.id)}
                    >
                    <PlayArrowIcon />
                </IconButton>
            </CardActions>
        </Card>
    );
}