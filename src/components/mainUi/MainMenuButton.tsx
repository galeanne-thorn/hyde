import React from "react";
import { useDispatch } from "react-redux";
import setMode from '../../model/mainUi/setModeAction';
import { UiMode } from '../../model/mainUi/UiState';
import MenuButton from "./MenuButton";

/**
 * Options menu button
 * @param props
 * @returns
 */
export default function MainMenuButton() {

    const dispatch = useDispatch();

    return (
        <MenuButton
            onClick={() => dispatch(setMode(UiMode.MainMenu))}
        >
            Main Menu
        </MenuButton>
    );
}
