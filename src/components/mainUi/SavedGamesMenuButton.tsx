import React from "react";
import { useDispatch } from "react-redux";
import setMode from "../../model/mainUi/setModeAction";
import { UiMode } from "../../model/mainUi/UiState";
import MenuButton from "./MenuButton";
import SaveIcon from '@material-ui/icons/Save';
/**
 * Properties for Save Menu Button
 */
export interface SaveMenuButtonProps {
    /**
     * Any save exists?
     */
    saveExists: boolean;
    /**
     * Is game in progress?
     */
    gameInProgress: boolean;
}

/**
 * Button for opening Save menu
 * @param props
 * @returns
 */
export default function SaveMenuButton(props: SaveMenuButtonProps) {

    const dispatch = useDispatch();

    return (
        <MenuButton
            color={
                props.gameInProgress
                ? "primary"
                : undefined
            }
            disabled={!(props.saveExists || props.gameInProgress)}
            startIcon={<SaveIcon />}
            onClick={() => dispatch(setMode(UiMode.SaveMenu))}
        >
            Saved Games
        </MenuButton>
    );
}
