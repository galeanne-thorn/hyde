import FastForwardIcon from '@material-ui/icons/FastForward';
import React from "react";
import { useDispatch } from "react-redux";
import loadLatestGame from "../../model/loadLatestGameAction";
import setMode from "../../model/mainUi/setModeAction";
import { UiMode } from "../../model/mainUi/UiState";
import MenuButton from "./MenuButton";

/**
 * Properties for ContinueGameButton
 */
export interface ContinueGameButtonProps {
    /**
     * Any save exists?
     */
    saveExists: boolean;
    /**
     * Is game in progress?
     */
    gameInProgress: boolean;
}

/**
 * Main Menu Continue Button
 * @param props
 * @returns
 */
export default function ContinueGameButton(props: ContinueGameButtonProps) {

    const dispatch = useDispatch();

    const doContinue = props.gameInProgress
        ? () => dispatch(setMode(UiMode.Game))
        : () => dispatch(loadLatestGame());

    const text = props.gameInProgress
        ? "Continue Game"
        : "Load and Continue Game";

    return (
        <MenuButton
            disabled={!props.gameInProgress && !props.saveExists}
            color="secondary"
            startIcon={<FastForwardIcon />}
            onClick={doContinue}
        >
            {text}
        </MenuButton>
    );
}
