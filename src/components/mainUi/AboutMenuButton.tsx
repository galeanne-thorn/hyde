import SettingsIcon from '@material-ui/icons/Settings';
import React from "react";
import { useDispatch } from "react-redux";
import setMode from '../../model/mainUi/setModeAction';
import { UiMode } from '../../model/mainUi/UiState';
import MenuButton from "./MenuButton";
import InfoIcon from '@material-ui/icons/Info';

/**
 * Menu Info Button
 * @param props
 * @returns
 */
export default function AboutMenuButton() {

    const dispatch = useDispatch();

    return (
        <MenuButton
            startIcon={<InfoIcon />}
            onClick={() => dispatch(setMode(UiMode.AboutMenu))}
        >
            About
        </MenuButton>
    );
}
