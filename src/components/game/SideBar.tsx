import { createStyles, IconButton, makeStyles, Paper, Theme, Tooltip, Typography } from "@material-ui/core";
import React from "react";
import { useDispatch } from "react-redux";
import { Game, perform, useGameState } from "../../model/game";
import { hourToDayPart } from "../../model/game/time";
import CharacterAvatar from "./CharacterAvatar";
import IconAwesome from "./IconAwesome";
import { SetUiStage } from "./UiStages";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: "flex",
            flexDirection: "column",
            justifyContent: "flex-start",
            alignItems: "center",
            backgroundColor: theme.palette.primary.main,
            height: "100%",
            margin: 0,
            padding: theme.spacing(1),
        },
    }),
);

/**
 * Side bar properties
 */
export interface SideBarProps {
    /**
     * Game to show
     */
    game: Game;
}

export default function SideBar(props: SideBarProps) {

    const classes = useStyles();
    const dispatch = useDispatch();

    const gameState = useGameState(s => s);

    const hero = props.game.hero.getActivePersona(gameState);

    const hour = gameState.time.hour.toString().padStart(2, "0");
    const minute = gameState.time.minute.toString().padStart(2, "0");
    const dayPart = hourToDayPart(gameState.time.hour);

    return (
        <Paper elevation={0} className={classes.root} >
            <CharacterAvatar url={hero.avatarUrl} big />
            <Typography variant="subtitle1">
                {gameState.hero.personas[hero.id].firstName} {hero.lastName}
            </Typography>
            <Typography variant="subtitle2">
                Day {gameState.time.day}
            </Typography>
            <Typography variant="subtitle2">
                {hour}:{minute}
            </Typography>
            <Typography variant="subtitle2">
                <em>{dayPart}</em>
            </Typography>
            <Tooltip title="Quests">
                <IconButton
                    aria-label="quests"
                    onClick={() => dispatch(perform(SetUiStage.QUESTS))}
                >
                    <IconAwesome iconId="Book" />
                </IconButton>
            </Tooltip>
        </Paper>
    );
}