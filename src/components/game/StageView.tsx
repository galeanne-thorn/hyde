import React from "react";
import { Game, STAGE_UI, useGameState } from "../../model/game";
import { STAGE_MAP } from "../../model/game/map";
import { STAGE_ROOM } from "../../model/game/rooms";
import { STAGE_SCENE } from "../../model/game/scene";
import MapStageView from "./MapStageView";
import QuestsView from "./QuestsView";
import RoomStageView from "./RoomStageView";
import SceneStageView from "./SceneStageView";
import { UiSceneIds } from "./UiStages";

/**
 * Stage view properties
 */
export interface StageViewProps {
    /**
     * Game to show
     */
    game: Game;
}

/**
 * Stage display component
 */
export default function StageView(props: StageViewProps) {

    const gameState = useGameState(s => s);
    const stageId = gameState.stages[0];

    if (stageId === undefined) {
        throw new Error(`No stage selected!`);
    }

    const stage = props.game.getStage(stageId);

    switch (stage.type) {
        case STAGE_SCENE:
            return <SceneStageView game={props.game} state={gameState} stageId={stageId} />
        case STAGE_MAP:
            return <MapStageView game={props.game} state={gameState} stageId={stageId} />
        case STAGE_ROOM:
            return <RoomStageView game={props.game} state={gameState} stageId={stageId} />

        case STAGE_UI:
            switch (stageId) {
                case UiSceneIds.QUESTS:
                    return <QuestsView game={props.game} state={gameState} stageId={stageId} />
                default:
                    throw new Error(`Unsupported UI stage ${stageId}`);
            }

        default:
            throw new Error(`Unsupported stage type ${stage.type}`);
    }
}