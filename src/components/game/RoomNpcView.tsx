import { createStyles, makeStyles, Paper, Theme, Typography } from "@material-ui/core";
import React from "react";
import { useDispatch } from "react-redux";
import { Game, GameState, Npc, perform } from "../../model/game";
import CharacterAvatar from "./CharacterAvatar";

/**
 * Properties of RoomNpcView
 */
export interface RoomNpcViewProps {
    /**
     * Game data
     */
    game: Game;
    /**
     * Game state
     */
    state: GameState;
    /**
     * NPC
     */
    npc: Npc;
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            marginBottom: theme.spacing(1),
            display: "flex",
            flexDirection: "column",
            justifyContent: "flex-start",
            alignItems: "center",
            padding: theme.spacing(1),
        },
    }),
);

/**
 * View of NPC in room
 */
export default function RoomNpcView(props: RoomNpcViewProps) {

    const classes = useStyles();
    const dispatch = useDispatch();

    const persona = props.npc.getActivePersona(props.state);

    const who = props.npc.getActivePersonaName(props.state);
    const dialog = props.npc.getDialogChoice(props.state);
    return (
        <Paper
            elevation={1}
            className={classes.root}
            onClick={() => dispatch(perform(dialog.id))}
        >
            <CharacterAvatar url={persona.avatarUrl} />
            <Typography variant="subtitle1">
                {who}
            </Typography>
        </Paper>
    );
}