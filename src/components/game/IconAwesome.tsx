import React from "react";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { SvgIcon, SvgIconProps } from "@material-ui/core";

/**
 * Properties for IconAwesome
 */
export interface IconAwesomeProps extends SvgIconProps {
    /** Icon Id */
    iconId: string;
}

/**
 * Creates material-ui SvgIcon from Font Awesome icon
 * @param props
 * @returns
 */
export default function IconAwesome(props: IconAwesomeProps) {

    const { iconId, ...rest } = props;

    const id = iconId.startsWith("fa")
        ? iconId
        : `fa${iconId}`;

    const def = fas[id];
    if (def === undefined) {
        throw new Error(`Icon ${id} not found!`);
    }

    const width = def.icon[0];
    const height = def.icon[1];
    const paths = (typeof (def.icon[4]) === "string")
        ? [def.icon[4]]
        : def.icon[4];

    return (
        <SvgIcon
            {...rest}
            width={width}
            height={height}
            viewBox={`0 0 ${width} ${height}`}>
            {paths.map((p, i) => <path key={i} d={p} />)}
        </SvgIcon>
    );
}
