import { Avatar, createStyles, makeStyles, Theme } from "@material-ui/core";
import React from "react";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        avatar: {
            width: "96px",
            height: "96px",
            margin: theme.spacing(1),
            marginBlock: "auto",
            backgroundColor: theme.palette.secondary.main,
        },
        avatarLarge: {
            width: "128px",
            height: "128px",
            margin: theme.spacing(1),
            marginBlock: "auto",
            backgroundColor: theme.palette.secondary.main,
        }
    }),
);

/**
 * Properties for {@see SceneMessage}
 */
 export interface CharacterAvatar {
    /**
     * Avatar URL
     */
    url?: string;

    /**
     * Use big avatar?
     */
    big?: boolean;
}

/**
 * Displays character avatar
 *
 * @param props Properties
 * @returns
 */
export default function CharacterAvatar(props: CharacterAvatar) {

    const classes = useStyles();

    const className = props.big
        ? classes.avatarLarge
        : classes.avatar;

    return (
        <Avatar
            variant="rounded"
            className={className}
            src={props.url}
        />
    );
}