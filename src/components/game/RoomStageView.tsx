import { createStyles, makeStyles, Theme } from "@material-ui/core";
import React from "react";
import { Room } from "../../model/game/rooms";
import Choices from "./Choices";
import RoomNpcView from "./RoomNpcView";
import StageDescription from "./StageDescription";
import StagePart from "./StagePart";
import { StageViewProps } from "./StageViewProps";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: "flex",
            flexDirection: "column",
            justifyContent: "flex-start",
            maxWidth: "60rem",
            margin: "auto",
            height: "100%"
        },
    })
);

/**
 * Room stage display
 * @param props
 */
export default function RoomStageView(props: StageViewProps) {

    const classes = useStyles();
    const room = props.game.getStage<Room>(props.stageId);

    // Get NPCs with location in this room
    const npcs = Object.entries(props.state.npcs)
        .filter(kv => kv[1].location === props.stageId)
        .map(kv => props.game.getNpc(kv[0]));
    const npcsView = npcs.map(n =>
        <RoomNpcView
            key={n.id}
            npc={n}
            state={props.state}
            game={props.game}
        />
    );
    const npcChoices = npcs.map(n => n.getDialogChoice(props.state));

    return (
        <div className={classes.root}>
            <StageDescription
                title={room.name}
                text={room.description}
                game={props.game}
                state={props.state}
            />
            <StagePart>
                {...npcsView}
            </StagePart>
            <Choices
                choices={npcChoices}
                game={props.game}
                state={props.state}
            />
            <Choices
                choices={room.choices}
                game={props.game}
                state={props.state}
            />
        </div>
    );
}
