import { createStyles, makeStyles, Theme } from "@material-ui/core";

/**
 * CSS Animation styles
 */
export const useAnimationStyles = makeStyles((theme: Theme) =>
    createStyles({
        "fadeIn": {
            animation: `$myEffect 1500ms ${theme.transitions.easing.easeInOut}`
        },
        "@keyframes myEffect": {
            "0%": {
                opacity: 0,
            },
            "100%": {
                opacity: 1,
            }
        },
    }),
);

export default useAnimationStyles;