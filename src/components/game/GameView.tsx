import { createStyles, makeStyles, Theme } from "@material-ui/core";
import React from "react";
import { Game } from "../../model/game";
import SideBar from "./SideBar";
import StageView from "./StageView";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            height: "90vh"
        },
        sidebar: {
            alignSelf: "stretch",
            padding: theme.spacing(1),
            minWidth: "25ch",
            height: "100%"
        },
        stage: {
            maxWidth: "120ch",
            minWidth: "60ch",
            flexGrow: 3
        }
    }),
);

/**
 * Game view properties
 */
export interface GameViewProps {
    /**
     * Game to show
     */
    game: Game;
}

/**
 * Game view component
 */
export function GameView(props: GameViewProps) {

    const classes = useStyles();

    return (
        <div className={classes.root}>
            <div className={classes.sidebar}>
                <SideBar game={props.game} />
            </div>
            <div className={classes.stage}>
                <StageView game={props.game} />
            </div>
        </div>
    );
}