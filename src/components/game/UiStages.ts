import game from "../../game/Game";
import { addHandler, setStage, UiStage } from "../../model/game";

/**
 * Id of the quests stage
 */
export const UiSceneIds = {
    QUESTS: "__quests"
};

/**
 * IDs of SetStage handlers for UI stages
 */
export const SetUiStage = {
    QUESTS: "ui_stage_quest"
};

// Register stages
game.addStage(new UiStage(UiSceneIds.QUESTS, "Quests"));

// Register handlers
addHandler(SetUiStage.QUESTS, setStage(UiSceneIds.QUESTS));

