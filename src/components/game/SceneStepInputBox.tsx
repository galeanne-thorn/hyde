import { Button, createStyles, makeStyles, Paper, TextField, Theme, Typography } from "@material-ui/core";
import React, { useState } from "react";
import { Game, GameState, SceneStepInput } from "../../model/game";
import TemplateText from "../TemplateText";
import useAnimationStyles from "./AnimationStyles";
import CharacterAvatar from "./CharacterAvatar";
import usePerform from "./usePerform";

/**
 * Properties for {@see SceneStepReplyView}
 */
export interface SceneStepInputBoxProps {
    /**
     * Step to display
     */
    step: SceneStepInput;
    /**
     * Game definition
     */
    game: Game;
    /**
     * Game state
     */
    state: GameState;
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: "75%",
            marginBottom: theme.spacing(1),
            display: "flex",
            flexDirection: "row",
            justifyContent: "flex-start",
            alignSelf: "flex-end"
        },
        message: {
            display: "flex",
            flexDirection: "column"
        },
        button: {
            margin: theme.spacing(1),
        }
    }),
);

/**
 * Display of scene step Reply
 * @param props Properties
 * @returns
 */
export default function SceneStepInputBox(props: SceneStepInputBoxProps) {

    const classes = useStyles();
    const animation = useAnimationStyles();

    const perform = usePerform();

    const [value, setValue] = useState(props.step.defaultValue);

    const submitInput = () => {
        perform(props.step.handlerId, value);
    }

    // TODO: Get Hero info for color and avatar
    const persona = props.game.hero.getActivePersona(props.state);

    return (
        <Paper
            elevation={1}
            className={`${classes.root} ${animation.fadeIn}`}
        >
            <CharacterAvatar url={persona.avatarUrl} />
            <div className={classes.message}>
                <Typography variant="subtitle1">
                    You
                </Typography>
                <TemplateText
                    template={props.step.text}
                    game={props.game}
                    state={props.state}
                />
                <TextField
                    autoFocus
                    value={value}
                    onChange={e => setValue(e.target.value)}
                    onKeyDown={e => { if (e.key === "enter") submitInput() }}
                />
                <Button
                    className={classes.button}
                    color="secondary"
                    variant="outlined"
                    onClick={submitInput}
                >
                    Reply
                </Button>
            </div>
        </Paper>
    );
}