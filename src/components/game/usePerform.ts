import { useDispatch } from "react-redux";
import { perform } from "../../model/game";

/**
 * Shortcut hook for performing dispatch(perform())
 *
 * @param handlerId - Id of the handler
 * @param data - Optional handler data
 */
export default function usePerform() {
    const dispatch = useDispatch();
    return (handlerId: string, data?: unknown) => dispatch(perform(handlerId, data));
}