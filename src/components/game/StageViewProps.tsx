import { Game } from "../../model/game/Game";
import { GameState } from "../../model/game/GameState";

/**
 * Stage view properties
 */
export interface StageViewProps {
    /**
     * Game data
     */
    game: Game;
    /**
     * Game state
     */
    state: GameState;
    /**
     * Current stage Id
     */
    stageId: string;
}
