import { createStyles, makeStyles, Theme } from "@material-ui/core";
import React, { useMemo } from "react";
import { useDispatch } from "react-redux";
import { perform } from "../../model/game/GameAction";
import { MapStage } from "../../model/game/map";
import Choices from "./Choices";
import IconSymbol from "./IconSymbol";
import StageDescription from "./StageDescription";
import { StageViewProps } from "./StageViewProps";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: "flex",
            flexDirection: "column",
            justifyContent: "flex-start",
            maxWidth: "60rem",
            margin: "auto",
            height: "100%"
        },
        map: {
            margin: theme.spacing(1),
            alignSelf: "center",
        },
        location: {
            fill: theme.palette.info.light,
            "&:hover": {
                fill: theme.palette.primary.light,
            },
            "& use": {
                fill: theme.palette.common.white
            },
            "&:hover use": {
                fill: theme.palette.common.black,
            },
        },
    }),
);

/**
 * Map Stage View
 * @param props
 */
export default function MapStageView(props: StageViewProps) {

    const classes = useStyles();
    const dispatch = useDispatch();

    const map = props.game.getStage<MapStage>(props.stageId);

    const choices = useMemo(
        () => map.choices
            .filter(ch => ch.condition?.(props.state, props.game) ?? true),
        [props.stageId, props.state]
    );

    // Get icon symbols
    const iconDefs = useMemo(
        () => choices
            .map(ch => ch.icon)
            .filter((v, i, s) => s.indexOf(v) === i)
            .map(id => <IconSymbol key={id} id={id} />),
        [choices]
    );

    const mapIcons = choices
        .map(ch => (
            <svg
                key={`${ch.x}-${ch.y}`}
                x={ch.x}
                y={ch.y}
                className={classes.location}
                onClick={() => dispatch(perform(ch.id))}
            >
                <title>{ch.text}</title>
                <circle cx="24.5" cy="24.5" r="25" />
                <use
                    href={`#${ch.icon}`}
                    width="48"
                    height="48"
                />
            </svg>
        ));

    return (
        <div className={classes.root}>
            <StageDescription
                title={map.name}
                text={map.text}
                game={props.game}
                state={props.state}
            />
            <svg
                className={classes.map}
                viewBox="0 0 1280 720"
            >
                {...iconDefs}
                <image href={`assets/map/${map.image}`} x="0" y="0" width="1280" height="720" />
                {...mapIcons}
            </svg>
            <Choices
                choices={map.choices}
                game={props.game}
                state={props.state}
            />
        </div>
    );
}
