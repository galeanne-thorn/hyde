import React from "react";
import { fas } from "@fortawesome/free-solid-svg-icons";

/**
 * Properties for IconSymbol
 */
export interface IconSymbolProps {
    /** Icon Id */
    id: string;
}

/**
 * Creates svg symbol from Font Awesome icon
 * @param props
 * @returns
 */
export default function IconSymbol(props: IconSymbolProps) {

    const id = props.id.startsWith("fa")
        ? props.id
        : `fa${props.id}`;
    const def = fas[id];
    if (def === undefined) {
        throw new Error(`Icon ${id} not found!`);
    }
    const width = def.icon[0];
    const height = def.icon[1];
    const paths = (typeof (def.icon[4]) === "string")
        ? [def.icon[4]]
        : def.icon[4];

    return (
        <symbol
            id={props.id}
            width={width}
            height={height}
            viewBox={`0 0 ${width} ${height}`}>
            {paths.map((p, i) => <path key={i} d={p} />)}
        </symbol>
    );
}
