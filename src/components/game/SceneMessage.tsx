import { createStyles, makeStyles, Paper, Theme, Typography } from "@material-ui/core";
import React from "react";
import { Game } from "../../model/game/Game";
import { GameState } from "../../model/game/GameState";
import TemplateText from "../TemplateText";
import useAnimationStyles from "./AnimationStyles";
import CharacterAvatar from "./CharacterAvatar";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: "75%",
            marginBottom: theme.spacing(1),
            display: "flex",
            flexDirection: "row",
            justifyContent: "flex-start"
        },
        alignStart: {
            alignSelf: "flex-start",
        },
        alignEnd: {
            alignSelf: "flex-end"
        },
        message: {
            display: "flex",
            flexDirection: "column",
            padding: theme.spacing(1)
        },
    }),
);

/**
 * Properties for {@see SceneMessage}
 */
export interface SceneMessageProps {
    /**
     * Who is talking
     */
    who: string;
    /**
     * The message
     */
    text: string;
    /**
     * Game definition
     */
    game: Game;
    /**
     * Game state
     */
    state: GameState;
    /**
     * Alignment
     */
    alignment: "start" | "end";
    /**
     * Avatar URL
     */
    avatarUrl?: string;
}

/**
 * Display of scene step when hero or NPC talks
 * @param props Properties
 * @returns
 */
export default function SceneMessage(props: SceneMessageProps) {

    const classes = useStyles();
    const animation = useAnimationStyles();

    const alignment = props.alignment === "start"
        ? classes.alignStart
        : classes.alignEnd;

    return (
        <Paper
            elevation={1}
            className={[classes.root, alignment, animation.fadeIn].join(" ")}
        >
            <CharacterAvatar url={props.avatarUrl} />
            <div className={classes.message}>
                <Typography variant="subtitle1">
                    {props.who}
                </Typography>
                <TemplateText
                    template={props.text}
                    game={props.game}
                    state={props.state}
                />
            </div>
        </Paper>
    );
}