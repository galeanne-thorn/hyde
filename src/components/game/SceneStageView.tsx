import { createStyles, makeStyles, Paper, Theme } from "@material-ui/core";
import React, { useEffect } from "react";
import {
    ADVANCE_SCENE, Scene, SceneStep, SceneStepInput, SceneStepReply, SceneStepSay,
    SCENE_STEP_INPUT, SCENE_STEP_REPLY, SCENE_STEP_SAY
} from "../../model/game/scene";
import SceneStepNarration, { SCENE_STEP_NARRATION } from "../../model/game/scene/SceneStepNarration";
import Choices from "./Choices";
import SceneStepInputBox from "./SceneStepInputBox";
import SceneStepNarrationView from "./SceneStepNarrationView";
import SceneStepReplyView from "./SceneStepReplyView";
import SceneStepSayView from "./SceneStepSayView";
import { StageViewProps } from "./StageViewProps";
import usePerform from "./usePerform";
import options from "../../Options";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: "flex",
            flexDirection: "column",
            justifyContent: "space-between",
            maxWidth: "60rem",
            margin: "auto",
            height: "100%"
        },
        scene: {
            display: "flex",
            flexDirection: "column",
            backgroundColor: theme.palette.primary.dark,
            margin: theme.spacing(1),
            padding: theme.spacing(1),
        }
    }),
);

/**
 * Scene stage display
 * @param props
 */
export default function SceneStageView(props: StageViewProps) {

    const perform = usePerform();

    const scene = props.game.getStage<Scene>(props.stageId);
    const sceneState = scene.getOrCreateState(props.state, props.game);

    const classes = useStyles();

    function stepView(step: SceneStep, key: number) {
        switch (step.type) {
            case SCENE_STEP_NARRATION:
                return <SceneStepNarrationView
                    key={key}
                    step={step as SceneStepNarration}
                    game={props.game}
                    state={props.state}
                />;
            case SCENE_STEP_SAY:
                return <SceneStepSayView
                    key={key}
                    step={step as SceneStepSay}
                    game={props.game}
                    state={props.state}
                />;
            case SCENE_STEP_REPLY:
                return <SceneStepReplyView
                    key={key}
                    step={step as SceneStepReply}
                    game={props.game}
                    state={props.state}
                />;
            case SCENE_STEP_INPUT:
                return undefined; // No output unless it is current step
            default:
                return (
                    <div key={key}>
                        {`Unknown Step ${step.type}`}
                    </div>
                );
        }
    }

    // Show previous steps
    const steps = sceneState.doneSteps
        .map(id => stepView(scene.steps[id], id));

    // Handle input step separately
    const currentStepId = sceneState.currentStep
    const currentStep = scene.steps[currentStepId];
    if (currentStep.type === SCENE_STEP_INPUT) {
        steps.push(
            <SceneStepInputBox
                key={currentStepId}
                step={currentStep as SceneStepInput}
                game={props.game}
                state={props.state}
            />
        );
    }
    else {
        // Add current step
        steps.push(stepView(currentStep, currentStepId));
    }

    // If there are no choices and we are not at the end of the scene,
    // automatically advance to next scene step

    useEffect(
        () => {
            const autoAdvance =
                currentStep.type !== SCENE_STEP_INPUT
                && (currentStep.choices === undefined || currentStep.choices.length === 0)
                && sceneState.currentStep < scene.steps.length - 1
                ;
            if (autoAdvance) {
                setTimeout(
                    () => perform(ADVANCE_SCENE),
                    options.stepDelay
                );
            }
        },
        [sceneState.currentStep, props.stageId]
    );

    return (
        <div className={classes.root}>
            <Paper elevation={0} className={classes.scene}>
                {...steps}
            </Paper>
            <Choices
                choices={currentStep.choices}
                game={props.game}
                state={props.state}
            />
        </div>
    );
}
