import { createStyles, makeStyles, Paper, Theme, Typography } from "@material-ui/core";
import React from "react";
import { Game, GameState } from "../../model/game";
import TemplateText from "../TemplateText";
import StagePart from "./StagePart";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        title: {
            textAlign: "center",
        },
        text: {
            backgroundColor: theme.palette.primary.main,
            margin: theme.spacing(1),
            padding: theme.spacing(1),
            alignSelf: "center",
        }
    })
);

/**
 * Properties for the stage description
 */
export interface StageDescriptionProps {
    /**
     * Optional title
     */
    title?: string;
    /**
     * Text to show
     */
    text: string;
    /**
     * Game data
     */
    game: Game;
    /**
     * Game state
     */
    state: GameState;
}

/**
 * Simple stage description component
 * @param props
 * @returns
 */
export default function StageDescription(props: StageDescriptionProps) {
    const classes = useStyles();

    const title = props.title === undefined
        ? null
        : (
            <Typography variant="h5" className={classes.title} >
                {props.title}
            </Typography>
        );
    return (
        <StagePart>
            {title}
            <Paper elevation={1} className={classes.text}>
                <TemplateText
                    template={props.text}
                    game={props.game}
                    state={props.state}
                />
            </Paper>
        </StagePart>
    );
}