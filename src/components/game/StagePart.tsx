import { createStyles, makeStyles, Paper, Theme } from "@material-ui/core";
import React from "react";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        background: {
            backgroundColor: theme.palette.primary.dark,
            margin: theme.spacing(1),
            padding: theme.spacing(1),
        }
    })
);

/**
 * Properties for the stage description
 */
export interface StagePartProps {
    /**
     * Children to show
     */
    children: React.ReactNode;
}

/**
 * Simple stage description component
 * @param props
 * @returns
 */
export default function StagePart(props: StagePartProps) {

    const classes = useStyles();

    return (
        <Paper elevation={0} className={classes.background}>
            {props.children}
        </Paper>
    );
}