import React from "react";
import { SceneStepSay } from "../../model/game/scene";
import { Game } from "../../model/game/Game";
import { GameState } from "../../model/game/GameState";
import SceneMessage from "./SceneMessage";

/**
 * Properties for {@see SceneStepSayView}
 */
export interface SceneStepSayViewProps {
    /**
     * Step to display
     */
    step: SceneStepSay;
    /**
     * Game definition
     */
    game: Game;
    /**
     * Game state
     */
    state: GameState;
}

/**
 * Display of scene step Say
 * @param props Properties
 * @returns
 */
export default function SceneStepSayView(props: SceneStepSayViewProps) {

    const npc = props.game.getNpc(props.step.who);
    const persona = npc.getActivePersona(props.state);

    const who = npc.getActivePersonaName(props.state);

    return (
        <SceneMessage
            alignment="start"
            who={who}
            avatarUrl={persona.avatarUrl}
            text={props.step.text}
            game={props.game}
            state={props.state}
        />
    );
}