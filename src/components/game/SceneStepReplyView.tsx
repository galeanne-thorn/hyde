import React from "react";
import { SceneStepReply } from "../../model/game/scene";
import { Game } from "../../model/game/Game";
import { GameState } from "../../model/game/GameState";
import SceneMessage from "./SceneMessage";

/**
 * Properties for {@see SceneStepReplyView}
 */
export interface SceneStepReplyViewProps {
    /**
     * Step to display
     */
    step: SceneStepReply;
    /**
     * Game definition
     */
    game: Game;
    /**
     * Game state
     */
    state: GameState;
}

/**
 * Display of scene step Reply
 * @param props Properties
 * @returns
 */
export default function SceneStepReplyView(props: SceneStepReplyViewProps) {

    const persona = props.game.hero.getActivePersona(props.state);

    return (
        <SceneMessage
            alignment="end"
            who="You"
            text={props.step.text}
            avatarUrl={persona.avatarUrl}
            game={props.game}
            state={props.state}
        />
    );
}