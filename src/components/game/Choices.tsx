import { Button, createStyles, makeStyles, Theme } from "@material-ui/core";
import React from "react";
import { useDispatch } from "react-redux";
import { Choice, Game, GameState, perform } from "../../model/game";
import { evaluateTemplate } from "../TemplateText";

/**
 * Properties for Choices
 */
export interface ChoicesProps {
    /**
     * Choices to render
     */
    choices: Array<Choice> | undefined;
    /**
     * Game definition
     */
    game: Game;
    /**
     * Game state
     */
    state: GameState;
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            marginTop: theme.spacing(1),
            display: "flex",
            justifyContent: "flex-end",
            flexWrap: "wrap",
        },
        button: {
            margin: theme.spacing(1),
            flexGrow: 1
        },
    }),
);

/**
 *
 * @param props - properties
 * @returns
 */
export default function Choices(props: ChoicesProps) {

    const dispatch = useDispatch();
    const classes = useStyles();

    if (props.choices === undefined) return null;

    const choices = props.choices
        .filter(ch => ch.condition?.(props.state, props.game) ?? true)
        .map(ch => (
            <Button
                key={ch.id}
                className={classes.button}
                variant="contained"
                onClick={() => dispatch(perform(ch.id))}
            >
                {evaluateTemplate(ch.text, props.state, props.game)}
            </Button>
        ));

    return (
        <div className={classes.root}>
            {...choices}
        </div>
    );
}