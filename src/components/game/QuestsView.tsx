import { Button, createStyles, makeStyles, Tab, Tabs, Theme, Typography } from "@material-ui/core";
import React, { ChangeEvent, useState } from "react";
import { useDispatch } from "react-redux";
import { perform, POP_STAGE, QuestStatus } from "../../model/game";
import QuestView from "./QuestView";
import StagePart from "./StagePart";
import { StageViewProps } from "./StageViewProps";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: "flex",
            flexDirection: "column",
            justifyContent: "flex-start",
            maxWidth: "60rem",
            margin: "auto",
            height: "100%"
        },
        button: {
            margin: theme.spacing(1),
        },
    })
);

/**
 * Shows quests
 */
export default function QuestsView(props: StageViewProps) {

    const classes = useStyles();
    const dispatch = useDispatch();

    const [selectedTab, setTab] = useState(0);

    const handleChange = (event: ChangeEvent<{}>, newValue: number) => {
        setTab(newValue);
    };

    let wanted: QuestStatus;
    switch (selectedTab) {
        case 0: // active
            wanted = QuestStatus.Active;
            break;
        case 1: // finished
            wanted = QuestStatus.Success;
            break;
        case 2: // failed
            wanted = QuestStatus.Failed;
            break;
    }

    const quests = Object.entries(props.state.quests)
        .filter(v => v[1].status === wanted)
        .map(v =>
            <QuestView
                key={v[0]}
                quest={props.game.getQuest(v[0])}
                state={v[1]}
            />
        );

    const placeholder = quests.length > 0
        ? null
        : <StagePart>No quests found</StagePart>
        ;

    return (
        <div className={classes.root}>
            <Typography variant="h4" align="center">
                Quests
            </Typography>
            <Tabs value={selectedTab} onChange={handleChange} >
                <Tab label="Active" />
                <Tab label="Finished " />
                <Tab label="Failed" />
            </Tabs>
            {placeholder}
            {...quests}
            <Button
                className={classes.button}
                variant="contained"
                onClick={() => dispatch(perform(POP_STAGE))}
            >
                Close
            </Button>
        </div>
    );
}