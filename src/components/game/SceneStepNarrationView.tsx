import { createStyles, makeStyles, Paper, Theme } from "@material-ui/core";
import React from "react";
import { SceneStepNarration } from "../../model/game/scene";
import { Game } from "../../model/game/Game";
import { GameState } from "../../model/game/GameState";
import TemplateText from "../TemplateText";
import useAnimationStyles from "./AnimationStyles";

/**
 * Properties for {@see SceneStepNarrationView}
 */
export interface SceneStepNarrationViewProps {
    /**
     * Step to display
     */
    step: SceneStepNarration;
    /**
     * Game definition
     */
    game: Game;
    /**
     * Game state
     */
    state: GameState;
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: "100%",
            backgroundColor: theme.palette.primary.main,
            marginBottom: theme.spacing(1),
            padding: theme.spacing(1),
            alignSelf: "center",
        },
    })
);

/**
 * Display of scene step Narration
 * @param props Properties
 * @returns
 */
export default function SceneStepNarrationView(props: SceneStepNarrationViewProps) {

    const classes = useStyles();
    const animation = useAnimationStyles();
    return (
        <Paper
            elevation={0}
            className={`${classes.root} ${animation.fadeIn}`}
        >
            <TemplateText
                template={props.step.text}
                game={props.game}
                state={props.state}
            />
        </Paper>
    );
}