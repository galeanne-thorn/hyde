import { createStyles, List, ListItem, ListItemIcon, ListItemText, makeStyles, Paper, Theme } from "@material-ui/core";
import { green, red } from '@material-ui/core/colors';
import Check from "@material-ui/icons/Check";
import CheckBoxOutline from '@material-ui/icons/CheckBoxOutlineBlank';
import Clear from "@material-ui/icons/Clear";
import React from "react";
import { Quest, QuestState, QuestStatus } from "../../model/game";
import StagePart from "./StagePart";

/**
 * Quest view properties
 */
export interface QuestViewProps {
    /**
     * Quest to show
     */
    quest: Quest;
    /**
     * State of quest
     */
    state: QuestState;
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        main: {
            backgroundColor: theme.palette.primary.main,
            margin: theme.spacing(1),
            padding: theme.spacing(1),
            alignSelf: "center",
        },
        task: {
            margin: theme.spacing(1),
            padding: theme.spacing(1),
            alignSelf: "center",
        },
        success: {
            color: green[500],
        },
        failed: {
            color: red[900],
        },
    })
);

/**
 * Shows one quest
 */
export default function QuestView(props: QuestViewProps) {

    const classes = useStyles();

    function TaskStatus(status: QuestStatus) {
        switch (status) {
            case QuestStatus.Success:
                return <Check className={classes.success} />;
            case QuestStatus.Failed:
                return <Clear className={classes.failed} />;
            default:
                return null;
        }
    }

    const tasks = Object.entries(props.state.tasks)
        .filter(v => v[1].status !== QuestStatus.Inactive)
        .map(v =>
            <ListItem key={v[0]}>
                <ListItemIcon>
                    {TaskStatus(v[1].status)}
                </ListItemIcon>
                <ListItemText>
                    {props.quest.tasks.get(v[0])?.description}
                </ListItemText>
            </ListItem>
        );

    return (
        <StagePart>
            <Paper elevation={1} className={classes.main}>
                {props.quest.description}
            </Paper>
            <Paper elevation={1} className={classes.task}>
                <List dense>
                    {...tasks}
                </List>
            </Paper>
        </StagePart>
    );
}