import { AppBar, Container, createStyles, IconButton, makeStyles, Toolbar, Typography } from "@material-ui/core";
import MenuIcon from '@material-ui/icons/Menu';
import React from "react";
import { useDispatch } from "react-redux";
import { Game } from "../model/game";
import { useUi } from "../model/mainUi/Selectors";
import setMode from "../model/mainUi/setModeAction";
import { UiMode } from "../model/mainUi/UiState";
import ErrorBoundary from "./ErrorBoundary";
import { GameView } from "./game/GameView";
import AboutMenu from "./mainUi/AboutMenu";
import MainMenu from "./mainUi/MainMenu";
import OptionsMenu from "./mainUi/OptionsMenu";
import SaveMenu from "./mainUi/SaveMenu";

// This is imported via webpack Define Plugin
declare var __PACKAGE_VERSION__: string;

const useStyles = makeStyles(() =>
    createStyles({
        title: {
            width: "100%",
            textAlign: "center",
        },
    }),
);

/**
 * GameApp properties
 */
export interface GameAppProps {
    /**
     * Game to play
     */
    game: Game;
}

/**
 * Main application
 * @returns
 */
export default function GameApp(props: GameAppProps) {

    const dispatch = useDispatch();

    const classes = useStyles();

    const mode = useUi(s => s.mode);

    let screen = getScreen(mode, props.game);

    return (
        <Container>
            <AppBar position="static">
                <Toolbar>
                    <IconButton
                        disabled={mode === UiMode.MainMenu}
                        onClick={() => dispatch(setMode(UiMode.MainMenu))}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h1" className={classes.title}>
                        {props.game.name}
                    </Typography>
                    <Typography variant="overline">
                        {__PACKAGE_VERSION__}
                    </Typography>
                </Toolbar>
            </AppBar>
            <ErrorBoundary>
                {screen}
            </ErrorBoundary>
        </Container>
    );
}

/**
 * Gets game screen based on UI mode
 */
function getScreen(mode: UiMode, game: Game) {

    switch (mode) {
        case UiMode.MainMenu:
            return <MainMenu />;
        case UiMode.SaveMenu:
            return <SaveMenu game={game}/>;
        case UiMode.OptionsMenu:
            return <OptionsMenu />;
        case UiMode.AboutMenu:
            return <AboutMenu />;
        case UiMode.Game:
            return <GameView game={game} />
        default:
            return <div>{`Unsupported UI mode ${mode} (yet)`}</div>;
    }

}