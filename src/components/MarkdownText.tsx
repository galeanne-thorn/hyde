import DomPurify from "dompurify";
import { marked } from "marked";
import React from "react";
/**
 * Markdown properties
 */
export interface MarkdownTextProps {
    /**
     * Markdown Template to display
     */
    template: string;
}

/**
 * Markdown display
 */
export default function MarkdownText(props: MarkdownTextProps) {

    // TODO: Sanitize?
    const html = {
        __html: DomPurify.sanitize(marked(props.template))
    };

    return (
        <div dangerouslySetInnerHTML={html} />
    );
}
