import { compile, Options, TemplateFunction } from "ejs";
import React from "react";
import { Game } from "../model/game/Game";
import { GameState } from "../model/game/GameState";
import MarkdownText from "./MarkdownText";

/**
 * Markdown properties
 */
export interface TemplateTextProps {
    /**
     * Text of the template
     */
    template: string;
    /**
     * Game definition
     */
    game: Game;
    /**
     * Game state
     */
    state: GameState;
}

/**
 * EJS options
 */
const ejsOptions: Options = {
    openDelimiter: "{",
    closeDelimiter: "}",
};

/**
 * Compiles and applies the template
 * @param template
 * @param state
 * @param game
 *
 * @returns Final text
 */
export function evaluateTemplate(text: string, state: GameState, game: Game) {
    const locals = {
        g: game,
        s: state,
    };
    // The cast is required so TS does not comply about Promise variant
    const template = compile(text, ejsOptions) as TemplateFunction;
    return template(locals);
}

/**
 * Template text display as markdown
 */
export default function TemplateText(props: TemplateTextProps) {
    const text = evaluateTemplate(props.template, props.state, props.game);
    return <MarkdownText template={text} />
}
