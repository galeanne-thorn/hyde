import { createMuiTheme, responsiveFontSizes } from "@material-ui/core/styles";

/**
 * Default theme used for the game
 */
export const theme = responsiveFontSizes(
    createMuiTheme({
        palette: {
            primary: {
                main: "#FAEBD7",
                light: "#FFFFFF",
                dark: "#C7B9A6",
                contrastText: "#000000",
            },
            secondary: {
                main: "#A36046",
                light: "#D78E72",
                dark: "#71351E",
                contrastText: "#FFFFFF",
            },
            background: {
                default: "#FFF8EF",
            },
        },
        typography: {
            fontSize: 18,
            h1: {
                fontFamily: "Niconne",
                fontSize: 40,
            },
            subtitle1: {
                fontFamily: "Niconne",
                fontSize: 32,
            },
        },

    })
);

export default theme;