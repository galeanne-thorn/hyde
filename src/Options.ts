import { storage } from "./persistence/GamePersister"

const OPTIONS_KEY = "game-options";

/**
 * Game options
 */
export interface Options {
    /**
     * Delay of scene steps auto-progress
     */
    stepDelay: number;
}

/**
 * Default options
 */
const defaultOptions: Options = {
    stepDelay: 1000
};

/**
 * Game options
 */
const options : Options = storage.load(OPTIONS_KEY) ?? defaultOptions;

/**
 * Saves options
 */
export function saveOptions() {
    storage.save(OPTIONS_KEY, options);
}

export default options;