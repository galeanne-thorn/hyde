import { Npc, Persona } from "../../../model/game/person";
import game from "../../Game";

game.addNpc(
    new Npc(
        "jane",
        new Persona(
            "jane",
            "Evans",
            {
                firstName: "Jane",
                title: "Mother",
                strangerName: "Madam"
            }
        )
        .withAvatar("assets/avatar/janeEvans.png")
    )
)