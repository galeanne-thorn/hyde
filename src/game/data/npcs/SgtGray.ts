import { addTime, combine, continueScene, isQuestActive, popStage, setStage, succeedQuestTask } from "../../../model/game";
import { Npc, Persona } from "../../../model/game/person";
import game from "../../Game";

game.addNpc(
    new Npc(
        "sgt-gray",
        new Persona(
            "policeman",
            "Gray",
            {
                firstName: "John",
                title: "Sgt.",
                strangerName: "Policeman"
            }
        )
            .asKnown()
            .withAvatar("assets/avatar/sgt-gray.png")
    )
        .atLocation("room-soho-police")
        .withDialogFor(
            "policeman",
            d => d
                .say("sgt-gray",
                    `Hi, {%=s.hero.personas.male.firstName%}.
                    What can I do for you?`
                )
                .withChoice(
                    "Say goodbye",
                    popStage
                )
                .withChoice(
                    "Ask about Mr. Hyde",
                    combine(
                        addTime(0, 5),
                        continueScene("hyde")
                    ),
                    isQuestActive("searchForHyde")
                )
                .nextStepLabel("hyde")
                .reply("I need some info on certain Mr. Hyde. Small, pale. Heard he may have record for some mischiefs here in Soho")
                .say("sgt-gray",
                    `Hey, you know the rules. No information to civilians. And sorry, but you are a civilian now.`)
                .reply("Comme on, you know me, John.")
                .say("sgt-gray",
                    `Yes, I do, but I do not want the inspector to see me talking with you, buddy.`)
                .reply("Yeah, I can imagine that. Thought he will calm down since I left.")
                .say("sgt-gray",
                    `You wish. Anyway, Mr. Hyde you say. It rings a bell. I might have run into that name few weeks back.`)
                .say("sgt-gray",
                    `You know what, screw the old bastard, lets go to the archives, but make sure no one sees you, OK?`
                )
                .reply("Sure. Thanks John.",)
                .withChoice(
                    "Move to archive",
                    setStage("hyde-search-police")
                )
        )
);
