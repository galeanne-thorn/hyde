import { Npc, Persona } from "../../../model/game/person";
import game from "../../Game";

game.addNpc(
    new Npc(
        "sgt-brady",
        new Persona(
            "policeman",
            "Brady",
            {
                firstName: "Garry",
                title: "Sgt.",
                strangerName: "Policeman"
            }
        )
        .asKnown()
        .withAvatar("assets/avatar/sgt-brady.png")
    )
);