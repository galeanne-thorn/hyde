import { Npc, Persona } from "../../../model/game/person";
import game from "../../Game";

game.addNpc(
    new Npc(
        "jekyll",
        new Persona(
            "hyde",
            "Hyde",
            {
                firstName: "Edward",
                title: "Mr."
            }
        ).withAvatar("assets/avatar/hyde.png")
    )
        .withPersona(
            new Persona(
                "jekyll",
                "Jekyll",
                {
                    firstName: "Henry",
                    title: "Dr."
                }
            )
                .withAvatar("assets/avatar/jekyll.png")
        )
);
