import { Npc, Persona } from "../../../model/game/person";
import game from "../../Game";

game.addNpc(
    new Npc(
        "guest",
        new Persona(
            "male",
            "Guest",
            {
                firstName: "Richard",
                title: "Mr.",
                strangerName: "client"
            }
        )
            .withAvatar("assets/avatar/guest.png")
    )
)