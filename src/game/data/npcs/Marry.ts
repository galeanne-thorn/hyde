import { Npc, Persona } from "../../../model/game/person";
import game from "../../Game";

game.addNpc(
    new Npc(
        "marry",
        new Persona(
            "marry",
            "Bright",
            {
                firstName: "Marry",
                title: "Miss",
                strangerName: "Prostitute"
            }
        )
            .withAvatar("assets/avatar/marryBright.png")
            .asKnown()
    )
)