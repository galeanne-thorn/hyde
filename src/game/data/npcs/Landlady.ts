import { Npc, Persona } from "../../../model/game/person";
import game from "../../Game";

game.addNpc(
    new Npc(
        "landlady",
        new Persona(
            "female",
            "Hawking",
            {
                firstName: "Tilda",
                title: "Mrs."
            }
        )
            .asKnown()
            .withAvatar("assets/avatar/landlady.png")
    )
)