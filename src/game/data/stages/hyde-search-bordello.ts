import { addShillings } from "../../../model/game/person";
import { Scene, SceneChoice } from "../../../model/game/scene";
import setStage from "../../../model/game/setStage";
import game from "../../Game";

game
    .addStage(
        new Scene(
            "hyde-search-bordello",
            "Search for Hyde - Bordello"
        )
        .narrate("You found Marry in the common room.")
        .reply("Hello. I was told you may know something about Mr. Hyde.")
        .say("marry", "I do not know much. Only that he is a major pain in ass, thats for sure.")
        .reply("Ah, that I know too, but I need something more specific, like where to find him.")
        .say("marry", "Asking about this man can be dangerous. Whats in for me?")
        .narrate("You sigh. Why is everybody asking for your precious money ...")
        .reply("Would one shilling disperse you fears?")
        .say("marry", "Maybe... Not all, you know...")
        .reply("Two then?")
        .say("marry", "Yes, that will help for sure")
        .withChoices(
            new SceneChoice(
                "Give Marry two shillings",
                s => s.hero.wallet = addShillings(s.hero.wallet, -2),
                setStage("hyde-search-bordello2")
            )
        )
    );
