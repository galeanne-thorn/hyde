import { succeedQuestTask } from "../../../model/game/quest/questHandlers";
import { Scene, SceneChoice } from "../../../model/game/scene";
import setStage from "../../../model/game/setStage";
import game from "../../Game";

game
    .addStage(
        new Scene(
            "hyde-search-bordello2",
            "Search for Hyde - Bordello"
        )
        .say("marry", "OK, I will tell you what I know.")
        .reply("I all one ear...")
        .say("marry",
`Well, Hyde can be charming, but looses his temper pretty fast.
He knows what he wants and goes for it over dead bodies, at least that is what I heard.
He likes expensive stuff and seem to have lots of grease, though he feels more like a scrooge to me, never gave me a tip, bastard.
In sex he likes it rough, but never crossed the line till yesterday.
`)
        .reply("What was the struggle yesterday about?")
        .say("marry",
`Bastard wanted to stick his prick into my ass.
I refused, of course, I am honest trooper, not some sodomite ...`)
        .reply("Yes, of course ...")
        .reply("Any clue where I can find him?")
        .say("marry", "I saw him few times on the Wardour street around the Royalty theatre, usually in the evenings")
        .reply("That does not help me much, lot of people hang out around Royalty...\nAnything else?")
        .say("marry", "Well, I know he has some beaf with the gang from the workhouse on Marshall street.")
        .say("marry", "Oh, and I heard father Jarvis complaining about him.")
        .reply("Father Jarvis, the priest from Saint Luke, right?")
        .say("marry", "Yes, that's him. ")
        .withChoices(
            new SceneChoice(
                "leave",
                succeedQuestTask("searchForHyde", "bordello"),
                setStage("map-search-for-hyde"))
        )
    );