import { makePersonaKnown, Scene, SceneChoice, setStage } from "../../../model/game";
import { addTime } from "../../../model/game/time";
import game from "../../Game";

game
    .addStage(new Scene(
        "prologue1",
        "Welcome Client"
    )
        .say("landlady", "**Cough! Cough!**")
        .narrate("The admonishing caught of my landlady, Mrs. {%=g.getNpc('landlady').getActivePersona(s).lastName%}, woke me up.")
        .say("landlady", "You have a guest, Mr. {%=g.hero.personas.get('male').lastName%}")
        .narrate("With that, she left and let in middle-aged men with dark hair in.")
        .narrate("I quickly dropped the blanket down to floor and stand up.")
        .reply("Welcome, mister ...?")
        .say("guest", "Mr. Robert Guest. Am I talking to Mr. {%=g.hero.personas.get('male').lastName%}?")
        .input(
            "That is correct. I am ",
            game.hero.personas.get("male")?.firstName ?? "Adam",
            (s, v) => s.hero.personas["male"].firstName = v
        )
        .reply("That is correct. I am {%=s.hero.personas.male.firstName%} {%=g.hero.personas.get('male').lastName%}")
        .withChoices(
            new SceneChoice(
                "Shake hands with Mr. Guest",
                makePersonaKnown("guest"),
                setStage("prologue2"),
                addTime(0, 5)
            )
        )
    );
