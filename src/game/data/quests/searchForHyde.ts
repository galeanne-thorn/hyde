import { activateQuestTask, Quest, QuestTask } from "../../../model/game/quest";
import game from "../../Game";

game.addQuest(
    new Quest(
        "searchForHyde",
        "You have to gather information about certain Mr.Hyde in Soho"
    )
        // TODO: Activate into the lions den quest
        // .onSuccessDo()
        .withTask(
            new QuestTask(
                "police",
                "Ask about Mr.Hyde in Soho police station"
            ).onSuccessDo(
                activateQuestTask("searchForHyde", "bordello"),
                activateQuestTask("searchForHyde", "boutique")
            ),
        )
        .withTask(new QuestTask(
            "parish",
            "Ask about Mr.Hyde in St.Luke parish"
        ))
        .withTask(new QuestTask(
            "pawnshop",
            "Ask about Mr.Hyde in Mr. Harris Pawnshop"
        ))
        .withTask(
            new QuestTask(
                "bordello",
                "Ask about Mr.Hyde in bordello on Little Chapel Street"
            )
            .onSuccessDo(
                activateQuestTask("searchForHyde", "workhouse")
            )
        )
        .withTask(
            new QuestTask(
                "boutique",
                "Ask about Mr.Hyde in boutique on Button Street"
            ).onSuccessDo(
                activateQuestTask("searchForHyde", "parish"),
                activateQuestTask("searchForHyde", "pawnshop")
            )
        )
        .withTask(new QuestTask(
            "workhouse",
            "Ask about Mr.Hyde in Soho workhouse"
        ))
)