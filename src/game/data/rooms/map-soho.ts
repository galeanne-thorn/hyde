import { MapStage } from "../../../model/game/map";
import { attributeExpPerLevel } from "../../../model/game/person/PersonAttributes";
import { defaultSkillLevel, skillToExperience } from "../../../model/game/person/SkillLevels";
import { isTaskActive } from "../../../model/game/quest/questHandlers";
import setStage from "../../../model/game/setStage";
import game from "../../Game";

game.addStage(
    new MapStage(
        "map-soho",
        "Soho",
        "Where should I go?",
        "soho.jpg"
    )
        .withLocation(
            250, 400,
            "Home",
            "Home",
            undefined,
            setStage("room-home-office")
        )
        .withLocation(
            350, 475,
            "Landmark",
            "Police Station",
            isTaskActive("searchForHyde", "police"),
            setStage("room-soho-police")
        )
        // .withLocation(
        //     550, 475,
        //     "Beer",
        //     "Pub",
        //     isTaskActive("searchForHyde", "meetJohn"),
        //     setStage("room-soho-pub")
        // )
        .withLocation(
            400, 300,
            "UsersCog",
            "Small Tom's hideout in the workhouse",
            isTaskActive("searchForHyde", "workhouse"),
            s => {
                s.hero.attributes.faith += attributeExpPerLevel; // Dogmatic
                s.hero.attributes.sexuality -= attributeExpPerLevel; // Pure
                s.hero.skills.crime = skillToExperience(defaultSkillLevel + 1);
            },
            setStage("hyde-search-gang")
        )
        .withLocation(
            510, 400,
            "Church",
            "Father's Jarvis parish house",
            isTaskActive("searchForHyde", "parish"),
            s => {
                s.hero.attributes.intelligence += attributeExpPerLevel; // Cunning
                s.hero.attributes.will += attributeExpPerLevel; // Dominant
                s.hero.skills.mysticism = skillToExperience(defaultSkillLevel + 1);
            },
            setStage("hyde-search-parish")
        )
        .withLocation(
            550, 200,
            "HandHoldingHeart",
            "Mother Evans nunnery",
            isTaskActive("searchForHyde", "bordello"),
            s => {
                s.hero.attributes.sexuality += attributeExpPerLevel; // Corrupt
                s.hero.attributes.will -= attributeExpPerLevel; // Submissive
                s.hero.skills.sex = skillToExperience(defaultSkillLevel + 1);
            },
            setStage("room-soho-bordello")
        )
        .withLocation(
            800, 150,
            "Cut",
            "Mrs. Thompson boutique",
            isTaskActive("searchForHyde", "boutique"),
            setStage("room-soho-boutique")
        )
        .withLocation(
            750, 350,
            "Coins",
            "Mr. Harris pawnshop",
            isTaskActive("searchForHyde", "pawnshop"),
            setStage("hyde-search-pawnshop")
        )
);
