import { setStage } from "../../../model/game";
import { Room, RoomExit } from "../../../model/game/rooms";
import game from "../../Game";

game
    .addStage(
        new Room(
            "room-soho-police",
            "Soho Police Station",
            "Soho Police station. I used to work there, before I got the idea to become a private detective."
        )
            .withExit(
                new RoomExit("Leave", setStage("map-soho"))
            )
    );