import { setStage } from "../../../model/game";
import { Room, RoomExit } from "../../../model/game/rooms";
import game from "../../Game";

game
    .addStage(
        new Room(
            "room-soho-bordello",
            "Mother Evans Nunnery",
            "One of the many Soho bordellos, this one being run by Mother Evans."
        )
            .withExit(
                new RoomExit("Leave", setStage("map-soho"))
            )
    );