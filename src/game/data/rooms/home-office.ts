import { setStage } from "../../../model/game";
import { Room, RoomExit } from "../../../model/game/rooms";
import game from "../../Game";

game
    .addStage(
        new Room(
            "room-home-office",
            "My Office",
            "This is my office. I have rented it when I decided to pursue the career of private detective"
        )
            .withExit(
                new RoomExit("Bedroom", setStage("room-home-office-bedroom"))
            )
            .withExit(
                new RoomExit("Go Out", setStage("map-soho"))
            )
    )
    .addStage(
        new Room(
            "room-home-office-bedroom",
            "My Bedroom",
            "This is my bedroom. Not much, but I can live here"
        )
            .withExit(
                new RoomExit("Office", setStage("room-home-office"))
            )
        // TODO: wardrobe, bed
    );