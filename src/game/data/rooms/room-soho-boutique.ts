import { setStage } from "../../../model/game";
import { Room, RoomExit } from "../../../model/game/rooms";
import game from "../../Game";

game
    .addStage(
        new Room(
            "room-soho-boutique",
            "Button Street Boutique",
            `
Boutique with french style clothing on the Soho border.

Luxury for Soho inhabitants`
        )
            .withExit(
                new RoomExit("Leave", setStage("map-soho"))
            )
    );