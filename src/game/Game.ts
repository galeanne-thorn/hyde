import { Game } from "../model/game/Game";

/**
 * Game instance
 */
const game = new Game("The Strange Case of Dr. Jekyll and Ms. Hyde");

export default game;