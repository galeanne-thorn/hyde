import { Game } from "../model/game/Game";
import game from "./Game";

/**
 * Gets the game
 * @returns Promise that returns game when all definitions are loaded
 */
export async function loadGame() : Promise<Game> {
    const context = require.context("./data", true, /\.(ts|scene)$/, "eager")
    const promises = context.keys().map(m => {
        import(
            /* webpackMode: "eager" */
            `./data/${m.substring(2)}`
        )
    });
    return Promise.all(promises)
        .then(() => game);

}