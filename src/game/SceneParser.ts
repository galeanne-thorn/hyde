import { continueScene, Scene, SceneChoice } from "../model/game/scene";
import setStage from "../model/game/setStage";

// Parsing regex
// Expects steps
// Capturing groups:
// 0 : label
// 1 : command label
// 2 : params
const regex = /^---(?:(.*)@)?(.)(?::(.*))?$/gm;

/**
 * Step data
 */
interface StepData {
    /** Label of the step */
    label?: string;
    /** Command / Step type */
    command: string;
    /** Command parameters as one string */
    parameters?: string;
    /** Command text (for UI) */
    text: string;
}

/**
 * Parses MD formatted text to simple scene steps without choices
 *
 * Expects scene steps to be split by following string starting
 * on the beginning of line:
 *
 * ---label@command:parameters
 *
 * If text does not start with the delimiter as above, it is assumed
 * the first step is narration
 *
 * @param id - Id of the scene
 * @param name - Name of the scene
 * @param text - Multi line text to parse
 *
 * @returns New scene
 */
export default function parseScene(
    id: string,
    name: string,
    text: string
): Scene {

    if (text.length <= 0) {
        throw new Error("Empty text to parse!");
    }

    const scene = new Scene(id, name);

    // Split text to scene steps
    const parts = text.split(regex);

    const steps: Array<StepData> = [];

    // First text is narration if not empty
    if (parts[0] !== "") {
        steps.push({
            command: "N",
            text: parts[0]
        });
    }
    for (let i = 1; i < parts.length; i += 4) {
        steps.push({
            label: parts[i],
            command: parts[i + 1],
            parameters: parts[i + 2],
            text: parts[i + 3]
        });
    }

    steps.forEach(s => {
        // Set step label
        if (s.label) {
            scene.nextStepLabel(s.label);
        }
        // Add step
        switch (s.command) {
            case "G":
                if (s.parameters === undefined) {
                    throw new Error(`Undefined stage for 'go to stage' choice!`);
                }
                scene.withChoices(
                    new SceneChoice(s.text, setStage(s.parameters))
                );
                break;
            case "J":
                if (s.parameters === undefined) {
                    throw new Error(`Undefined stage for 'continue to step' choice!`);
                }
                scene.withChoices(
                    new SceneChoice(s.text, continueScene(s.parameters))
                );
                break;
            case "N":
                scene.narrate(s.text);
                break;
            case "R":
                scene.reply(s.text);
                break;
            case "S":
                if (s.parameters === undefined) {
                    throw new Error(`Undefined who for scene step Say!`);
                }
                scene.say(s.parameters, s.text);
                break;
            default:
                throw new Error(`Unsupported scene command '${s.command}'`);
        }
    });

    return scene;
}