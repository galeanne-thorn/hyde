# The Strange Case of Dr. Jekyll and Ms. Hyde

This is work of fiction. Events and persons depicted in this work are not real and any similarity to existing person is purely coincidental.

## License

The Strange Case of Dr. Jekyll and Ms. Hyde © 2021 by Galeanne Thorn is licensed under [CC BY 4.0](http://creativecommons.org/licenses/by/4.0/)

Code is under MIT License:

> The MIT License (MIT)
>
> Copyright (c) 2021-2022 Galeanne Thorn
>
> Permission is hereby granted, free of charge, to any person obtaining a copy
> of this software and associated documentation files (the "Software"), to deal
> in the Software without restriction, including without limitation the rights
> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
> copies of the Software, and to permit persons to whom the Software is
> furnished to do so, subject to the following conditions:
>
> The above copyright notice and this permission notice shall be included in all
> copies or substantial portions of the Software.
>
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
> SOFTWARE.

### Fonts

#### Nicone

Copyright (c) 2011, Vernon Adams, with Reserved Font Name Nicone

This Font Software is licensed under the SIL Open Font License, Version 1.1.
This license is available with a FAQ at: http://scripts.sil.org/OFL

#### Roboto

by Google under Apache License, Version 2.0, January 2004
http://www.apache.org/licenses/

### Icons

#### Font Awesome

https://fontawesome.com/ is licensed under [CC BY 4.0](http://creativecommons.org/licenses/by/4.0/) licence

#### Material-UI Icons

https://material-ui.com/components/material-icons/ is licensed under [MIT](https://github.com/mui-org/material-ui/blob/next/LICENSE) license

### Graphics

#### London Maps

https://www.davidrumsey.com/

Edward Stanford Ltd.

Stanford's London atlas of universal geography exhibiting the physical and political divisions of the various countries of the world. Folio edition. One hundred maps, with a list of latitudes and longitudes. Second issue, revised and enlarged. London, Edward Stanford, Geographer to Her Majesty, 12, 13 & 14 Long Acre, W.C. 1901.
1901
