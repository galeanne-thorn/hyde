const path = require("path");

const outputPath = path.resolve(__dirname, "static");
const npmPackage = require("./package.json");
const version = JSON.stringify(npmPackage.version);

module.exports = {
  outputPath,
  version,
  config: {
    entry: [
      "./src/index.tsx",
    ],
    resolve: {
      extensions: [".tsx", ".ts", ".jsx", ".js", ".json"],
      fallback: {
        "fs": false,
        "path": false
      },
    },
    output: {
      filename: "hyde.js",
      path: outputPath,
    },
    experiments: {
      topLevelAwait: true,
    },
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          loader: "ts-loader",
          exclude: [/node_modules/, /src\/loaders/, /\.spec\.tsx?$/],
        },
        {
          test: /\.scene$/,
          use: [
            path.resolve(__dirname, "src/loaders/scene-loader.js")
          ]
        },
        {
          test: /\.md$/,
          type: "asset/source",
        },
      ]
    }
  }
};