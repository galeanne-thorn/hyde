module.exports = {
  root: true,
  parser: "@typescript-eslint/parser",
  env: {
    "browser": true,
    "node": true
  },
  plugins: [
    "react",
    "@typescript-eslint",
    "prettier",
  ],
  extends: [
    "eslint:recommended",
    "plugin:react/recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
    // Enables eslint-plugin-prettier and eslint-config-prettier.
    // This will display prettier errors as ESLint errors.
    // Make sure this is always the last configuration in the extends array.
    "prettier",
    // Uses eslint-config-prettier to disable ESLint rules
    // from other plugins that would conflict with prettier
    "prettier/@typescript-eslint"
  ],
  rules: {
    "prettier/prettier": "error",
    "quotes": ["error", "double"],
    "react/prop-types": "off",
    "@typescript-eslint/explicit-function-return-type": [
      "error",
      { allowExpressions: true }
    ]
  },
  settings: {
    react: {
      version: "detect"
    }
  }
};